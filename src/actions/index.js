import axios from 'axios'

export const VariablesInfo = {
    SHOW_LOGOS: "SHOW_LOGOS",
    SHOW_FOOTER: 'SHOW_FOOTER',
    SHOW_FOOTER2: 'SHOW_FOOTER2',
    SHOW_CARRUSEL: 'SHOW_CARRUSEL',
    SHOW_SLIDER: 'SHOW_SLIDER',
    SHOW_SLIDER2: 'SHOW_SLIDER2',
    SHOW_SLIDER_S: 'SHOW_SLIDER_S',
    SHOW_CARRUSEL_P: 'SHOW_CARRUSEL_P',
    SHOW_CATEGORIAS: 'SHOW_CATEGORIAS',
    SHOW_CATEGORIAS_M: 'SHOW_CATEGORIAS_M',
    SHOW_SLIDER_DP: 'SHOW_SLIDER_DP',
    SHOW_PRODUCTOS_CAT: 'SHOW_PRODUCTOS_CAT',
    SHOW_EXISTENCIA_TIENDA: 'SHOW_EXISTENCIA_TIENDA',
    DETALLE_PRODUCTO: 'DETALLE_PRODUCTO',
    DETALLE_PRODUCTO_TIPO: 'DETALLE_PRODUCTO_TIPO',
    DETALLE_PRODUCTO_MIGAS: 'DETALLE_PRODUCTO_MIGAS',
    DETALLE_PRODUCTO_IMG: 'DETALLE_PRODUCTO_IMG',
    DETALLE_PRODUCTO_VIDEO: 'DETALLE_PRODUCTO_VIDEO',
    DETALLE_PRODUCTO_ATTR: 'DETALLE_PRODUCTO_ATTR',
    DETALLE_PRODUCTO_SLIDER_IMG: 'DETALLE_PRODUCTO_SLIDER_IMG',
    DETALLE_PRODUCTO_FORMAS_PAGO: 'DETALLE_PRODUCTO_FORMAS_PAGO',
    DETALLE_PRODUCTO_COLORS_AND_TALLAS: 'DETALLE_PRODUCTO_COLORS_AND_TALLAS',
    DETALLE_PRODUCTO_TALLAS: 'DETALLE_PRODUCTO_TALLAS',
    CAMBIAR_E: 'CAMBIAR_E',
    CAMBIAR_E_BTN: 'CAMBIAR_E_BTN',
    COLOR_SELECCIONADO: 'COLOR_SELECCIONADO',
    COLOR_DEFAULT: 'COLOR_DEFAULT',
    SHOW_TIENDAS: 'SHOW_TIENDAS'

}

/* export const SHOW_LOGOS = "SHOW_LOGOS"

export const SHOW_FOOTER = 'SHOW_FOOTER'

export const SHOW_FOOTER2 = 'SHOW_FOOTER2' */


//Home

export function showLogos(){
    //esto se hace por el Middleware
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/lkd6o')
        .then((response)=> {
            dispatch( {type: VariablesInfo.SHOW_LOGOS, payload: response.data.data} )
        })
    }

}

export function showFooter(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/alrxs')
        .then((response)=> {
            dispatch( {type: VariablesInfo.SHOW_FOOTER, payload: response.data.data.content} )
        })
    }

}


export function showFooter2(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/izzkg')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_FOOTER2, payload: response.data.data.content})
        })
    }
}

export function showCarrusel(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/12d364')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_CARRUSEL, payload: response.data.data.content})
        })
    }
}

export function showSlider(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/ed3rw')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_SLIDER, payload: response.data.data.content})
        })
    }
} 

export function showSlider2(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/12d364')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_SLIDER2, payload: response.data.data.content})
        })
    }
}

export function showSliderSamborns(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/17rbgc')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_SLIDER_S, payload: response.data.data})
        })
    }
}

export function showCarruselP(){
    return (dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/1btnz0')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_CARRUSEL_P, payload: response.data.data})
        })
    }
}

export function showCategorias(){
    return (dispatch, getStore)=>{
        axios.get('https://api.myjson.com/bins/bwybg')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_CATEGORIAS, payload: response.data.data.content})
        })
    }
}

export function showSliderDP(){
    return(dispatch, getState)=>{
        axios.get('https://api.myjson.com/bins/1andio')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_SLIDER_DP, payload: response.data.data.content})
        })
    }
}

export function showProductosCat(){
    return(dispatch, getState) =>{
        axios.get('https://api.myjson.com/bins/1hgo2y')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_PRODUCTOS_CAT, payload: response.data.data.content})
        })
    }
}

//Detalle de producto
export function showExistenciaTienda(sku_p){
    return(dispatch, getState) =>{
        //axios.get('http://clauluna.com/CLARO/api/getRecogeTienda?skuP=180731&skuH=51927781')
        axios.get("http://clauluna.com/CLARO/api/getRecogeTienda?skuP="+sku_p+"&skuH=51927781")
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_EXISTENCIA_TIENDA, payload: response.data.data.content})
        })
    }
}


export function detalleProducto(id){
    //var liga = "http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"";
    //console.log(liga)
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        //axios.get("https://api.myjson.com/bins/im5z4")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO, payload: response.data.data})
        })
    }
}

export function detalleProductoTipo(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_TIPO, payload: response.data.data.type_product})
        })
    }
}

export function detalleProductoVideos(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_VIDEO, payload: response.data.data.videos})
        })
    }
}

export function detalleProductoMigajas(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_MIGAS, payload: response.data.data.categories})
        })
    }
}

export function detalleProductoImagenes(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_IMG, payload: response.data.data.images})
        })
    }
}

export function detalleProductoAttr(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_ATTR, payload: response.data.data.attr})
        })
    }
}

export function detalleProductoFormasPago(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_FORMAS_PAGO, payload: response.data.data.tabs[1].content})
        })
    }
}

export function detalleProductoSliderImg(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_SLIDER_IMG, payload: response.data.data.carousel.products})
        })
    }
}

export function detalleProductoColorsANDTallas(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.DETALLE_PRODUCTO_COLORS_AND_TALLAS, payload: response.data.data.colors})
        })
    }
}

export function detalleProductoColor1(id){
    return(dispatch, getState) =>{
        axios.get("http://clauluna.com/CLARO/api/getDetalleProductoSears/"+id+"")
        .then((response)=>{
            dispatch({type: VariablesInfo.COLOR_DEFAULT, payload: response.data.data.colors[0].color})
        })
    }
}

export function showTiendas(){
    return (dispatch, getStore)=>{
        /*https://beta.dev.sanborns.com.mx/producto/getSucursales/*/
        axios.get('https://www.sanborns.com.mx/producto/getSucursales/')
        .then((response)=>{
            dispatch({type: VariablesInfo.SHOW_TIENDAS, payload: response.data.sucursales})
        })
    }
}


export function cambio(){
    return(dispatch)=> {
        dispatch({type: VariablesInfo.CAMBIAR_E})
    };
}

export function cambioBonton(){
    return(dispatch)=>{
        dispatch({type: VariablesInfo.CAMBIAR_E_BTN});
    };
}

            
export function colorSeleccionado(evento){
    return(dispatch)=>{
        dispatch({type: VariablesInfo.COLOR_SELECCIONADO, payload: evento });
    };
}