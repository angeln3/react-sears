import React from 'react';
import Fragment from 'render-fragment';



class LowerFooter extends React.Component {

    componentDidMount = () => {
       
        window.onscroll = function (){
            // Obtenemos la posicion del scroll en pantalla
            var scroll = document.documentElement.scrollTop || document.body.scrollTop;
        
            // Realizamos la aparicion del boton cuando el scroll este en 100px
            if(scroll > 100){
                document.getElementById('boton-ir-arriba').style.opacity = 1;
                
            }else{
                document.getElementById('boton-ir-arriba').style.opacity = 0; 
                
            }
        }
        
    } 

    scrollTop = () =>{
        //window.scrollTo(0,0);
        // incrementally scroll up 100px window.scrollBy({ top: -100, behavior: 'smooth' });
        window.scroll({ top: 0, behavior: 'smooth' });
          
    }

    render() {
        return (
            <Fragment>
                <div className="disclaimerNotice"></div>
                    <div className="lowerfooter">
                        <div className="legales"><a href="/Legal/">Términos y condiciones</a>
                            <span>|</span><a href="/Aviso_privacidad/">Políticas de privacidad</a>
                    </div>
                </div>    
                <a href="#!" id="boton-ir-arriba" style={{display: "block", opacity: 0}} onClick={this.scrollTop}></a>
            </Fragment>    
                ); 
                }
            }
                        
export default LowerFooter;
  