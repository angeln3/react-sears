import React from 'react';

//componentes
import Registro from './Registro';
import UpperFooter from './UpperFooter';
import Logos from './Logos';
import LowerFooter from './LowerFooter';

//Redux
//import { connect } from 'react-redux'
//import { showLogos, showFooter, showFooter2} from '../../actions'


class Footer extends React.Component {

    /* componentWillMount(){
        this.props.showLogos();
        this.props.showFooter();
        this.props.showFooter2();
    }  */

    render() {

        const {
            logos, 
            infoFooter,
            infoFooter2
                } = this.props;
        
        return (
            <footer className="nhfooter">
            
                <Registro/>

                <UpperFooter infoFooter={infoFooter} infoFooter2={infoFooter2}/>

                <Logos logos={logos} />

                <LowerFooter/>

                
            </footer>
            ); 
        }
    }
                        


/* function mapStateToProps(state){
    return {
        logos: state.data.list,
        infoFooter: state.data.list2,
        infoFooter2: state.data.list3
    }
} */


export default Footer;
  

{/* <div className="scripts">
                    <link rel="stylesheet" href="/css/font-awesome.css"/>
                    <link rel="stylesheet" href="/css/lib/jquery.bxslider.css"/>
                    <link rel="stylesheet" href="/css/lib/jquery.fancybox.css"/>
                    <link rel="stylesheet" href="/css/lib/slick.css"/>
                    <link rel="stylesheet" href="/css/lib/slick-theme.css"/>
                </div>
                <a href="#!" id="boton-ir-arriba" style={{display: 'none'}}></a>
                <div id="modal-carrito">
                    <div className="check"><span className="ico-check"></span></div>
                    <h2>Se han añadido <span id="anadidos"></span> artículos a tu carrito. Ahora tienes <strong id="cart_count2"></strong> artículos en tu Carrito.</h2>
                    <ul className="popUp-btns">
                        <li><a href="/carrito/" className="IrCarrito">Ir a carrito</a></li>
                        <li><a href="#!" onClick="$.fancybox.close();" className="SeguirComp">Seguir comprando</a></li>
                    </ul>
                    <div className="container">
                        <div className="mod-5">
                            <h2>Tambien te puede interesar</h2>
                            <div id="recomendadosCarrito" className="rowCuadrosProdDetalle"></div>
                        </div>
                    </div>
                </div>
                <div id="modal-wishlist" className="modal-EstilosWish2" style={{display: 'none'}}>
                    <form action="/mi-cuenta/listadeseos" method="post" id="nameform" name="nameform">
                        <div className="WH-cont">
                            <input name="idWL" id="idWL" type="hidden" />
                            <div className="WH-img" id="vistaRcarouselWL"></div>
                            <div className="WH-text">
                                <input type="hidden" id="idsProductos" name="idsProductos" value="" />
                                <p>Agregaste 1 producto a tu lista de deseos</p>
                                <ul className="WH-btns">
                                    <li>
                                        <button type="submit">Ver lista</button>
                                    </li>
                                    <li><a href="#!" onClick="$.fancybox.close()" className="SC-b">Seguir comprando</a></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="compartirEmail" className="compartirEmail" style={{display: 'none'}}>
                    <h4>Compartir este artículo por e-mail</h4>
                    <div className="contElements">
                        <div className="contImage"><img id="image" src="" alt="imagen"/></div>
                        <div className="contDescrip">
                            <p id="contDescrip" className="strong">-------- ------------ ------------- -------------- ----------------- ---------</p>
                            <p id="de">------------</p>
                            <p><strong>Enlace: </strong><a href="" id="enlace">---------------</a></p>
                        </div>
                    </div>
                    <form className="compartirEm">
                        <fieldset>
                            <label>Para:</label>
                            <textarea id="para" placeholder="Introduce direcciones de email: pedro@ejemplo.com.mx, marisa@ejemplo.com.mx. No se utilizarán con fines promocionales."></textarea>
                        </fieldset>
                        <fieldset>
                            <input type="checkbox" checked="" />
                            <label>Enviarme una copia de este email</label>
                        </fieldset>
                        <fieldset>
                            <div className="redesSociales"></div>
                            <button type="button" className="btn-blue">Enviar email</button>
                        </fieldset>
                    </form>
                </div>
                <div id="compartirEmail2" className="compartirEmail" style={{display: 'none'}}>
                    <h4>Compartir esta categoria por e-mail</h4>
                    <div className="contElements">
                        <div id="imageCompCat" className="contImage"></div>
                        <div className="contDescrip">
                            <p id="contNombreCat" className="strong"></p>
                            <p>Claroshop </p>
                            <p><strong>Enlace: </strong></p>
                            <div id="enlaceCompartir"></div>
                            <p></p>
                        </div>
                    </div>
                    <form className="compartirEm">
                        <fieldset id="listaCorreosCat">
                            <label>Para:</label>
                            <input id="paraCatCompartida" name="paraCatCompartida" placeholder="Introduce direcciones de email: pedro@ejemplo.com.mx, marisa@ejemplo.com.mx.  No se utilizarán con fines promocionales." type="text" />
                        </fieldset>
                        <fieldset>
                            <div className="redesSociales"></div>
                            <input type="hidden" name="idcatCompartida" id="idcatCompartida" />
                            <button type="button" id="btnCatCompartida" className="btn-blue" onClick="comparteMail.btnCatCompartida();">Enviar email</button>
                        </fieldset>
                    </form>
                </div>
                <div id="modal-newsletter">
                    <div className="check"><span className="ico-check"></span></div>
                    <h2>¡Listo! Recibirás las mejores promociones antes que nadie.</h2><a href="#" className="closeNWL redBtn">Aceptar</a>
                
                </div> */}