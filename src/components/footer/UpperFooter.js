import React from 'react';

//import foter from '../../jsons/Productos';

//import foterD from '../../jsons/Departamentos';

//import Fragment from 'render-fragment';


class Footer extends React.Component {

    constructor(){
        super();
        this.state={
            nombreEtiqueta: ''
        }
    }

    showSubMenus = (e) =>{
        
        let Etiqueta = e.target.innerHTML;
        switch(Etiqueta){
            case Etiqueta:
                this.setState(()=>({nombreEtiqueta: Etiqueta}))
                document.getElementById(Etiqueta).classList.toggle('active');
                break;
            default:
                this.setState(()=>({nombreEtiqueta: ''}))
        }
    }

    /*componentDidMount(){
         const url = "https://api.myjson.com/bins/alrxs";

        fetch(url)
        .then(res => { return res.json() })
        .then(datos => {
                this.setState({ data: datos.data.content})
            })
        .catch(() => console.log("Can't access " + url + " response. Blocked by browser?"))

 */
       /*  const url2 = "https://api.myjson.com/bins/izzkg";

        fetch(url2)
        .then(res => { return res.json() })
        .then(datos => {
                this.setState({ data2: datos.data.content})
            })
        .catch(() => console.log("Can't access " + url2 + " response. Blocked by browser?"))
 
    } */

    render() {

        const {infoFooter, infoFooter2} = this.props;
        const {nombreEtiqueta} = this.state;

        //const {data, data2}= this.state;

        //data.map
        const menuF = infoFooter.map((datos,i)=>{
            const subtitles = Object.values(datos.subtitles);
            return (
                <div key={i} className={datos.class}>
                    <h4 id={datos.title} onClick={this.showSubMenus} className={nombreEtiqueta === datos.title ? 'active' : ''} >{datos.title}</h4>
                    <ul>
                        {subtitles.map((datos2,i)=>{
                            return (
                                <li key={i+1}>
                                    <a href={datos2.href}>{datos2.subtitle}</a>
                                </li>
                            )
                            })
                        }
                    </ul>
                </div>
            )
        });
        //data2.map
        const menuFD = infoFooter2.map((datos,i)=>{
            const subtitles = Object.values(datos.subtitles);
            return (
                <div key={i} className={datos.class}>
                    <h4 id={datos.title} onClick={this.showSubMenus} className={nombreEtiqueta === datos.title ? 'active' : ''} >{datos.title}</h4>
                    <ul>
                        {subtitles.map((datos2,i)=>{
                            const informacion = Object.values(datos2.informacion);
                            if(datos2.subtitle === "Síguenos:"){
                                return(
                                    <div key={i+1} className="redesLogos">
                                        <h5>{datos2.subtitle}</h5>
                                        <ul>
                                        {informacion.map((datos3)=>{
                                            const imagenes = Object.values(datos3.imagenes);
                                            return(
                                                imagenes.map((datos4,i)=>{
                                                   return(
                                                    <li key={i+2}>
                                                        <a href={datos4.href} target="_blank" rel="follow">
                                                            <img src={datos4.img} alt="Siguenos en Pinterest"/>
                                                        </a>
                                                    </li>
                                                   )
                              
                                                })
                                            );
                                        })}
                                        
                                    </ul>
                                    </div>
                                );
                            }else{
                                return(       
                                    <li key={i}>
                                        <h5>{datos2.subtitle}</h5>
                                        {informacion.map((datos3,i)=>{
                                            return(
                                                <p key={i+1}>{datos3.info}</p>
                                            );
                                        })}
                                        
                                    </li>
                                );
                            }
                            
                        })}
                        
                    </ul>

                </div>
            );
        });

        return (
            <div className="upperfooter">
                    {menuF}
                    {menuFD}
                </div>
            
                ); 
                }
            }
                        
export default Footer;
  