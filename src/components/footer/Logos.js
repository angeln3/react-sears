import React from 'react';

//import { Provider } from 'react-redux'
//import images from '../../jsons/Departamentos';

class Logos extends React.Component{


   /*  constructor(){
        super();
        this.state={
            data: []
        }
    }
 */

    /* componentDidMount(){
        const url = "https://api.myjson.com/bins/lkd6o";

      fetch(url)
      .then(res => { return res.json() })
      .then(datos => {
            this.setState({ data: datos.data })
        })
       .catch(() => console.log("Can't access " + url + " response. Blocked by browser?"))
    }  */


    render(){

        //const  {data} = this.state;
        const {logos} = this.props; 

        return(
           //data.map
            <div className="nhmetodospago">
                    <div className="mplogos">
                        {logos.map((datos,i)=>{
                            return(
                                <a key={i} href={datos.href} target="_blank" rel="noopener">
                                    <img src={datos.img} />
                                </a>
                            );
                        })}
                    </div>
            </div>
          
        );
    }
}


export default Logos;