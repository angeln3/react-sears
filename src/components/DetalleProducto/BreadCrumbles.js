import React from 'react';

import json from '../../jsons/Departamentos.json';


import Fragment from 'render-fragment';


class BreadCrumbles extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            share: false,
            data: json.data,
        }
    }

    showShare = () =>{
        this.setState((state)=>({share: !state.share}))
    }

    render(){

        const {share, data, data2} = this.state;

        const {dataDP, dataDPMig} = this.props; 


        //data.categories
          const migajas = dataDPMig.map((datos,i)=>{
            const catego_h = Object.values(datos.categoriasHijas);
            return (
                <Fragment key={i}>
                        
                    { catego_h.map((datos2,i)=>{
                        return (
                            <li key={i+1}> <a href={datos2.seo}>{datos2.name}</a></li>
                        );
                    })
                    
                    }
                    <li> <a href={dataDP.title}>{dataDP.title}</a></li>
                </Fragment>
            );
        });  
                            
        const ligaFaceboock = "http://www.facebook.com/sharer/sharer.php?display=page&amp;u=https://www.sanborns.com.mx/producto/"+dataDP.id+"/"+dataDP.seo+"/#&amp;t="+dataDP.title+"/ ";

        //const ligaFaceboock = "http://www.facebook.com/sharer/sharer.php?display=page&amp;u=https://www.sanborns.com.mx/producto/"+data.id+"/"+data.seo+"/#&amp;t="+data.title+"/ ";
        const ligaTwitter = "https://twitter.com/intent/tweet?text= "+dataDP.title+" https://www.sanborns.com.mx/producto/"+dataDP.id+"/"+dataDP.seo+"/ &amp;via=claroshop_com";
        const ligaId = "https://www.sanborns.com.mx/producto/"+dataDP.id+"/";
        const ligaWhats = "whatsapp://send?text= "+dataDP.title+" https://www.sanborns.com.mx/producto/"+dataDP.id+"/"+dataDP.seo+"/";

        return(
            <section className="moduleBredcrumb">
                    <div className="container">
                        <div className="breadcrumb">
                            <ul>
                                {migajas}
                               {/*  <li><a href="/">Inicio</a></li>
                                <li><a href="/categoria/10/tecnologia-y-electronica/"> Tecnología y Electrónica </a></li>
                                <li><a href="/categoria/1004/telefonia/"> Telefonía </a></li>
                                <li><a href="/categoria/100405/apple/"> Apple </a></li>
                                <li><a href="#!">iPhone 11 64 GB Color Blanco R9 (Telcel)</a></li> */}
                            </ul>
                        </div>
                        <div id="shareSocial" className={share ? 'shareProd show' : 'shareProd'} onClick={this.showShare}>
                            
                            <p>Compartir en:</p>
                            <ul>
                                <li><a href={ligaFaceboock} className="b-fb" target="_blank"></a></li>
                                            
                                <li><a href={ligaTwitter} className="b-tw" target="_blank"></a></li>
                                <li><a href={ligaId} className="b-print" target="_blank"></a></li>
                                <li><a href={ligaWhats} data-action="share/whatsapp/share" className="b-whats"></a></li>
                            </ul>
                        </div>
                    </div>
                </section>
        );
    }
}

export default BreadCrumbles;