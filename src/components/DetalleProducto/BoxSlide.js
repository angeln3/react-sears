import React from 'react';

import Slider from 'react-slick';



class BoxSlide extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nav1: null,
            nav2: null,
            cerrarModal: false,
            zoomImg: false,
            nombreImg: '',
            slideIndex: 0
        }
    }

    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2
        })
    }

    prevent = (e) => {
        //previene el revote hacia arriba en la pagina
        e.preventDefault();
        // obtenemos el data index de la imagen
        let dataIndex = e.target.getAttribute('data-slide-index');
        // ponemos la posicion del la imagen seleccionada en el slider principal
        this.slider.slickGoTo(dataIndex);


        //console.log(dataIndex);
    }

    showModal = () => {
        this.setState((state) => ({ cerrarModal: !state.cerrarModal }))
        //si existe el video
        var video = document.getElementById('videoModal');
            if(video){
                document.getElementById('videoModal').style.display = ('none');
                document.getElementById('imgPrincipal').style.display = ('block');
            }
    }

    showModalImg = (e) => {

        this.setState((state) => ({ cerrarModal: !state.cerrarModal }))

        let Etiqueta = e.target.src;

        //poner imagen principal en el modal
        document.getElementById('imgPrincipal').src = Etiqueta;

        //para cambiar el color de los li en el modal
        switch (Etiqueta) {
            case Etiqueta:
                this.setState(() => ({ nombreImg: Etiqueta }))
        }

    }

    showZoom = () => {
        this.setState((state) => ({ zoomImg: !state.zoomImg }))
    }

    cambioImgModal = (e) => {
        let Etiqueta = e.target.src;    

        //si existe el video
        var video = document.getElementById('videoModal');
        if(video){
            //comparar si el src es un video
            if (Etiqueta === 'https://www.sanborns.com.mx/css/img/imgPlayerVideo.png') {

                document.getElementById('videoModal').style.display = ('block');
                document.getElementById('imgPrincipal').style.display = ('none');


            }else{
                
                document.getElementById('videoModal').style.display = ('none');
                document.getElementById('imgPrincipal').style.display = ('block');

            }
        }  
        //poner imagen principal en el modal
        document.getElementById('imgPrincipal').src = Etiqueta;

        //para cambiar el color de los li en el modal
        switch (Etiqueta) {
            case Etiqueta:
                this.setState(() => ({ nombreImg: Etiqueta }))
        }

         // ponemos la posicion del la imagen seleccionada en el slider principal
        let dataIndex = e.target.getAttribute('data-slide-index');

        this.slider.slickGoTo(dataIndex);

    }




    render() {

        var settingsSlider1 = {
            //asNavFor: this.state.nav2,
            ref: slider => (this.slider = slider)
        }

        var settingsSlider2 = {
            slidesToShow: 5, //solo puede mostrar 5
            slidesToScroll: 1,
            verticalScrolling: false,
            vertical: true,
            infinite: false,
            beforeChange: (current, next) => this.setState({ slideIndex: next }),
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        vertical: false,
                        slidesToShow: 8, //solo puede mostrar 8
                        infinite: false
                    }
                }
            ]
        }



        //    console.log('estos son el numeo de categorias',Object.keys(categorias).length);
        const { cerrarModal, zoomImg, nombreImg } = this.state;

        // importante poner data-slide-index={i} a las imagenes para que cambie de un estado a otro

        const { 
            dataDP, 
            dataDPImg,
            dtaDPVid
        } = this.props;

        return (
            <div className="info-Product">
                <div className="media-container">
                    <div className="bx-wrapper" style={{ maxWidth: '627px' }}>
                        <div className="bx-viewport" aria-live="polite" style={{ width: '100%', overflow: 'hidden', position: 'relative', height: '395px' }} >
                            <ul className="carrusel-producto" >

                                <Slider {...settingsSlider1}>

                                    {dataDPImg.map((datos,i)=>{
                                        return (
                                                <li key={i} onClick={this.showModalImg} aria-hidden="false" style={{ float: 'left', listStyle: 'none', position: 'relative', width: '480px' }}>
                                                    <img src={datos.link} alt={dataDP.title} title={dataDP.title} data-zoom-image="" className="zoomOut" />
                                                </li>  
                                        );
                                        
                                    })}

                                    {dtaDPVid.map((datos,i)=>{
                                                    return (
                                                        <li key={i+1} onClick={this.showModalImg} aria-hidden="true" style={{ float: 'left', listStyle: 'none', position: 'relative', width: '480px' }}>
                                                            <iframe width="460" height="266" src={datos.link} allowFullScreen="allowfullscreen"></iframe>
                                                        </li>
                                                    );
                                                    
                                                })}


                                    <div style={{ display: 'none' }}>
                                    </div>
                                    <div style={{ display: 'none' }} >
                                    </div>
                                    <div style={{ display: 'none' }} >
                                    </div>

                                </Slider>

                            </ul>
                        </div>
                        <div className="bx-controls bx-has-controls-direction">
                            <div className="bx-controls-direction"><a className="bx-prev slick-prev" href="#">Prev</a><a className="bx-next" href="">Next</a></div>
                        </div>
                    </div>
                    <div className="bx-slide" style={{ maxWidth: '60px' }} >
                        <div className="bx-viewport slick-list draggable" aria-live="polite" style={{ width: '100%', overflow: 'hidden', position: 'relative', height: '60px' }}>
                            <div id="bx-pager" className='slick-track' style={{ position: 'relative' }} >


                                <Slider {...settingsSlider2}>

                                    
                                    {dataDPImg.map((datos,i)=>{
                                        return (
                                            
                                                <a key={i+2} onClick={this.prevent} data-slide-index={i} href="#" className="outborder active" aria-hidden="false" style={{ float: 'none', listStyle: 'none', position: 'relative', width: '543px' }} >
                                                    <img src={datos.link} data-slide-index={i} />
                                                </a>                                               
                                            
                                        );
                                    })}

                                    {dtaDPVid.map((datos,i)=>{
                                                    if(typeof datos !== 'undefined'){
                                                        const nImg = dataDPImg.length;
                                                        return (
                                                            <a key={i+3} onClick={this.prevent} data-slide-index={nImg} href="#" className="outborder" aria-hidden="true" style={{ float: 'none', listStyle: 'none', position: 'relative', width: '543px' }}>
                                                                <img src="https://www.sanborns.com.mx/css/img/imgPlayerVideo.png" data-slide-index={nImg} />
                                                            </a>
                                                        );
                                                        }
                                                    })}

                                    <div onClick={this.prevent} style={{ display: 'none' }}>
                                    </div>
                                    <div onClick={this.prevent} style={{ display: 'none' }}>
                                    </div>
                                    <div onClick={this.prevent} style={{ display: 'none' }}>
                                    </div>

                                </Slider>

                            </div>
                        </div>
                        {/* <div className="bx-controls bx-has-controls-direction">
                            <div className="bx-controls-direction"><a className="bx-prev disabled slick-prev" href="#" onClick={this.prevent}>Prev</a><a className="bx-next slick-next" href="#" onClick={this.prevent}>Next</a></div>
                        </div>   */}
                    </div>
                </div>

                <div className="modalZoom" style={{ display: cerrarModal ? 'block' : 'none' }}>
                    <div className="bodyZoom">
                        <div className="contZoomRelative">
                            <div className="closeZoom"><span onClick={this.showModal} className="closeMob">Atrás</span><span onClick={this.showModal} className="closeDes">X</span></div>
                                <div className="contentZoom" id="contentZoom">
                                    {dataDPImg.map((datos,i)=>{
                                        if(datos.order === 1){
                                            return(
                                                    <img key={i+4} onClick={this.showZoom} id="imgPrincipal" src={datos.link} className={zoomImg ? '' : 'zoomin'} />
                                                );
                                        }
                                    })}

                                    {dtaDPVid.map((datos,i)=>{
                                        const vid = dtaDPVid.length;
                                        if(vid ==! 0){
                                            return(
                                                <iframe key={i+5} id="videoModal" width="100%" height="100%" src={datos.link} allowFullScreen="allowfullscreen" style={{display:'none'}}></iframe>
                                            );
                                        }
                                    })}
                                    
                                </div>
                            <div className="navigateZoom">
                                <h2>{dataDP.title}</h2>

                                {/* <Slider {...settingsSlider3}>
                                            <li className={nombreImg === "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681879943.jpg" ? 'active' : nombreImg === '' ? 'active' : ''}><img onClick={this.cambioImgModal} src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681879943.jpg"/></li>
                                            <li className={nombreImg === "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681881687.jpg" ? 'active' : ''}><img onClick={this.cambioImgModal} src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681881687.jpg"/></li>
                                            <li className={nombreImg === "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681880833.jpg" ? 'active' : ''}><img onClick={this.cambioImgModal} src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681880833.jpg"/></li>
                                            <li className={nombreImg === "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681883599.jpg" ? 'active' : ''}><img onClick={this.cambioImgModal} src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/2005681883599.jpg"/></li>    
                                        </Slider> */}

                                <ul>
                                    {dataDPImg.map((datos,i)=>{
                                        return(
                                            <li key={i+6} className={nombreImg === datos.link ? 'active' : ''}><img onClick={this.cambioImgModal} src={datos.link} data-slide-index={i} /></li>
                                        );
                                    })}

                                    {dtaDPVid.map((i)=>{
                                        const vid = dtaDPVid.length;
                                        const nImg = dataDPImg.length;
                                        console.log('NUMERO DE IMAGENES',nImg);
                                        if(vid ==! 0){
                                            return(
                                                <li key={i+7} className={nombreImg === 'https://www.sanborns.com.mx/css/img/imgPlayerVideo.png' ? 'active' : ''}><img onClick={this.cambioImgModal} src='https://www.sanborns.com.mx/css/img/imgPlayerVideo.png' data-slide-index={nImg} /></li>
                                            );
                                        }
                                    })}
                                   
                                    
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BoxSlide;