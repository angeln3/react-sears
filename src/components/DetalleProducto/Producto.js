import React from 'react';
import BoxSlide from './BoxSlide';

//Redux
import { connect } from 'react-redux';

import Fragment from 'render-fragment';

//Acciones de Redux
import {
    showExistenciaTienda,
    cambio
    } from '../../actions'


class Producto extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            contador : 1
        }
    }

    componentDidMount(){
        let miFormulario = document.querySelector('#det_compra');
       
        if(miFormulario){
            miFormulario.c.addEventListener('keypress', function (e){
                if (!soloNumeros(e)){
                  e.preventDefault();
              }
            })
            
            //Solo permite introducir numeros.
            function soloNumeros(e){
                var key = e.charCode;
                console.log(key);
                return key >= 48 && key <= 57;
            }
        }
       

    }

   /*  aumentarContador = (stock)=>{
        const {contador} = this.state;
        if(contador < stock){
            this.setState((state)=>({contador: ++state.contador}))
        }

        <input onClick={this.aumentarContador(dataDP.stock)}  type="button" value="+" className="btnCount btnPlus"/>
    } */

    aumentarContador = ()=>{
        const {contador} = this.state;
        if(contador < 35){
            this.setState((state)=>({contador: ++state.contador}))
        }
    }

    decrementoContador = () =>{
        const {contador} = this.state;
        if(contador > 1){
            this.setState((state)=>({contador: --state.contador}))
        }
       
    }
    //funcion para poner un 1 cundo no haya nada en el input de cantidad
    onBlur = () => {
        var valor = document.getElementById('c').value;

        if(valor == "" || valor == "0" || valor == "00"){
            document.getElementById('c').value = 1;
        }
    }
    //funcion para aceptar solo haste 35 en el input de cantidad
     num = (e) => {
        const {value} = e.target;

        if(value > 35){
            this.setState(({contador: 35}))
        }
        else{
            this.setState(({contador: value}))
            }   
            
    } 

    scroll = () =>{
        //para scroll aumenta 620 
        //window.scrollBy({top: 620, behavior: 'smooth'});

        window.scroll({ top: 620, behavior: 'smooth' });
    }

    cambiaTabExistencia = ()=>{
        this.props.cambio();
        //window.scroll({ top: 620, behavior: 'smooth' });
    }

    verExistencias = (sku_p) =>{
        this.props.showExistenciaTienda(sku_p);
    }

    render(){

        const {contador} = this.state;

        const {dataDP,
             dataJson, 
             dataDPMig, 
             dataDPImg,
             dtaDPVid
            } = this.props;


       /*  const categorias = dataDP.categories.map((datos,i)=>{
            const catego_h = Object.values(datos.categoriasHijas);

            return ( {datos.name} ); 
        }); */

         const categorias = dataDPMig.map((datos)=>{
            const catego_h = Object.values(datos.categoriasHijas);
            return ( 
                 catego_h.map((datos2)=>{return datos2.name.replace(/,/g, "") +"/"})
                );
        })
    
        return(
            <div className="viewsNdescrip"> 

                <BoxSlide
                 dataJson={dataJson} 
                 dataDP={dataDP} 
                 dataDPImg={dataDPImg}
                 dtaDPVid={dtaDPVid}
                 />
                
                <div id="modalSuccess">
                    <h2>{dataDP.title}</h2>
                    <div className="titleEntrevista">
                    Autor: 
                    <h3></h3>
                    </div>
                    <p className="descripEntrevista">
                        {dataDP.description}
                    </p>
                    <ul>
                    <li><iframe width="460" height="266" src="https://www.youtube.com/embed/_sJ-pTiIplI"  allowFullScreen="allowfullscreen" className="entrevistaAutor"></iframe></li>
                    </ul>
                </div>
                <div className="buy-Details">
                    <div className="miniDescrip borderBtm textRaw">
                    <h4>Descripción:</h4>
                    <p id="verCompleto">
                        {dataDP.description} ... 
                    </p>
                    <div className="link" id="link" onClick={this.scroll}>Ver más</div>
                    <p></p>
                    </div>
                    {dataDP.stock == 0 || dataDP.status == false ? '' : 
                    <form name="det_compra" id="det_compra" method="get">
                        <input name="a" id="a" type="hidden" value="1"/>
                        <input name="id" type="hidden" value={dataDP.id}/>
                        <input name="nombre" type="hidden" value={dataDP.title}/>
                        <input name="tienda" type="hidden" value="Sanborns"/>
                        <input name="precio" type="hidden" value={dataDP.sale_price}/>
                        <input name="precioInt" id="precioInt" type="hidden" value={dataDP.sale_price}/>
                        <input name="marca" type="hidden" /* value={dataDP.attributes.marca} *//>
                        <input name="categoria" type="hidden" value= {categorias} />
                        <input name="num_parte" type="hidden" value={dataDP.id}/>
                        <input name="idprod" id="idprod" type="hidden" value={dataDP.id}/>
                        <div className="cantidadOpt borderBtm">

                         
                            <a href="#btnProdSucursales" target="_top" style={{textDecoration:'none'}}> 
                                <div className="existenciaTiendas" onClick={()=>{this.cambiaTabExistencia(); this.verExistencias(dataDP.id)}}>Ver existencia en tiendas</div>
                            </a>  
                            
                                <div className="moduloCompra">
                                    <h3>Cantidad</h3>
                                    <div className="fieldset">
                                        <div className="countBox">
                                            <input onClick={this.decrementoContador} type="button" value="-" className="btnCount btnMinus"/>
                                            <input type="text" maxLength="2" min="1" max={dataDP.stock} value={contador} className="countValue" name="c" defaultValue="1" onChange={this.num} onBlur={this.onBlur} id="c"/>
                                            <input onClick={this.aumentarContador}  type="button" value="+" className="btnCount btnPlus"/>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </form>}
                </div>
        </div>
        );
    }
}

function mapStateToProps(state){
    return {
        NameEtic: state.nombreETIC
    }
}

export default connect(mapStateToProps,{showExistenciaTienda, cambio})(Producto);


