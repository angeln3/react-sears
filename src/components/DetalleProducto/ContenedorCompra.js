import React from 'react';
import Fragment from 'render-fragment';

//Redux
import { connect } from 'react-redux';

//Acciones de Redux
import {
    colorSeleccionado
    } from '../../actions'

class ContenedorCompra extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            colorSeleccionado: ""
        }
    }

    componentWillReceiveProps(prop){
        //resivimos el primer color por derecto para que haga la comparacion con la talla pro no cambia 
        //this.setState(()=>({colorSeleccionado: prop.datosDP_colorDef}));
    }


    abrirModalCarrito = () =>{
        document.querySelector('#modalC').style.display='block';
        document.querySelector('#modalCarro').style.display='block';
    }

    abreModal2 =() =>{
        document.getElementById('modalTienda').style.display = 'block';
        document.getElementById('modalC2').style.display = 'block'; 
        var colorSeleccionado = document.getElementById('colorSelect').selectedIndex;
        var tallaSeleccionada = document.getElementById('tallaSelect').selectedIndex;

        console.log('COLOR SELECCIONADO', colorSeleccionado, 'TALLA SELECCIONADA', tallaSeleccionada);

        document.getElementById("colorCR1").selectedIndex = colorSeleccionado;
        document.getElementById("tallaCR1").selectedIndex = tallaSeleccionada;

        var colorSeleccionadoM = document.getElementById('colorSelect').selectedIndex;
        var tallaSeleccionadaM = document.getElementById('tallaSelect').selectedIndex;

        console.log('COLOR SELECCIONADO MODAL', colorSeleccionadoM, 'TALLA SELECCIONADA MODAL', tallaSeleccionadaM);
    }

    cambioChange = (e) =>{

        this.setState({colorSeleccionado: e.target.value});

        this.props.colorSeleccionado(e.target.value);

    }



    obtenerSkuH = (e) =>{
        var selectTalla = document.getElementById("tallaSelect");
        var selected = selectTalla.options[selectTalla.selectedIndex].tabIndex;  
        console.log('ESTE ES EL SKU HIJO----',selected);

        console.log("INDEX___", selectTalla.selectedIndex);
    }

   /*  componentDidMount(){
        var combo = document.getElementById("tallaSelect");
        var selected = combo.options[combo.selectedIndex].text;  
        console.log(selected);
    } */

    render(){

        const {dataJson,
             dataDPTipo,
             datosDP_CyT,
             datosDP_colorDef
            } = this.props;

        const {dataDP} = this.props;

             
            console.log(datosDP_colorDef);

        const tipo_de_producto = () =>{
            //producto recoge en tienda
            if(dataDPTipo.name === "Click y recoge"){
                return (
                    <Fragment>

                        <button id="comprar" type="submit" className="btn-compra2">Comprar Ahora</button>
                        <a href="#" >
                            <button href="#modal-carrito" className="btn-paypal" type="button" id="buttonOneClick">Compra en 1 Click</button>
                        </a>
                        <button onClick={this.abrirModalCarrito} id="addCarrito"  href="#modal-carrito" className="btn-carrito slidcar" type="button">Agregar a Carrito</button>
                        
                        <button id="btnClickRecoge" type="button" className="btn-click" onClick={this.abreModal2}>Recoge en Tienda</button>

                        <p className="notaSmaller">*Disfruta los beneficios de Recoge en Tienda, consulta aquí las tiendas participantes.</p>
                    </Fragment>
                );
              //producto agotado  
            }else if(dataDP.stock == 0 || dataDP.status == false){
                return(
                    <Fragment>
                        <div id="mensajeAgotado" class="mensajeAgotado">
                            <div class="alertaAgotado">
                                <span>Producto no disponible<br></br><br></br>
                                El artículo no se encuentra disponible temporalmente. 
                                </span>
                            </div>
                        </div>
                    </Fragment>
                );

            }else{
                //POR LO MIENTRAS PONEMOS LAS OCBIONES DEL SELEC Y EL BOTON DE CLICK Y RECOGE
                return(
                    <Fragment>

                        <div class="opciones"><div class="boxColor">
                            <span>Color</span>
                            <div class="opc_colors">
                                <select name="color" id="colorSelect" onChange={this.cambioChange} >
                                    {datosDP_CyT.map((datos,i)=>{
                                        return(
                                            <option key={i} value={datos.color}>{datos.color}</option>
                                        );
                                    })}
                                    </select>
                            </div>
                        </div>
                        <div class="boxTallas">
                            <span>Tallas</span>
                            <div class="opc_sizes">
                                <select name="talla" id="tallaSelect"  onChange={this.obtenerSkuH} onClick={this.obtenerSkuH}>
                                {
                                           (datosDP_CyT.map((tal) => {
                                            return (tal.sizes.map((ta, it) => {
                                                return (
                                                    <Fragment key={it}>
                                                        {(tal.color === this.state.colorSeleccionado)
                                                            ?
                                                            <option key={it} value={tal.sizes[it].size} selected={this.state.bandera} tabIndex={tal.sizes[it].sku} > {tal.sizes[it].size} - {tal.sizes[it].sku}</option>
                                                            : null
                                                        }
                                                    </Fragment>
                                                );
                                            }))
                                        }))}
                                    </select>
                                    </div>
                            </div>
                        </div>

                        <button id="comprar" type="submit" className="btn-compra2">Comprar Ahora</button>
                        <a href="#" >
                            <button href="#modal-carrito" className="btn-paypal" type="button" id="buttonOneClick">Compra en 1 Click</button>
                            </a>
                         <button onClick={this.abrirModalCarrito} id="addCarrito"  href="#modal-carrito" className="btn-carrito slidcar" type="button">Agregar a Carrito</button>
                         <button id="btnClickRecoge" type="button" className="btn-click" onClick={this.abreModal2}>Recoge en Tienda</button>
                    </Fragment>

                );
                
            }

        }

        return(
            
            <div className="purchaseModule">
               
                    {dataDP.stock == 0 || dataDP.status == false ? '' :
                    <div className="price borderBtm" >
                        {dataDP.discount > 0 ? 
                        <p class="antes">Antes: $ {dataDP.price}</p>
                            : ''
                        }
                        <p className="total" id="precioCambio">$
                             {dataDP.sale_price}
                            <span> MXN</span>
                        </p>
                        {dataDP.discount > 0 ? 
                        <p class="ahorra">Ahorra: $ {dataDP.price - dataDP.sale_price}</p>
                            : ''
                        }
                    </div>}
                
                <div className="purchase_btns">
                    <div className="fieldset">
                        
                        {tipo_de_producto()}

                    </div>
                </div>
                <div className="box_Entrega">
                    <div className="boxTiempo">
                    <h2>Entrega</h2>
                    <div className="contEnvio">
                        <i className="envioGratis"></i>
                        <p className="tAprox">Aprox. 4 a 7 días hábiles*</p>
                    </div>
                    <p className="min">*Una vez autorizado el pago</p>
                    </div>
                    <div className="skuTienda">
                    <p>SKU#. {dataDP.sku} </p>
                    </div>
                    <div className="skuTienda">
                    <p>EAN#. {dataDP.ean} </p>
                    </div>
                </div>
                
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        productosSDP: state.data.list10,
        cambioCOL: state.cambioCol.cambioCOLOR

    }
}

//aqui lo conectamos
export default connect(mapStateToProps,
    {colorSeleccionado
    })(ContenedorCompra);

