import React from 'react';

//slider
import Slider from 'react-slick'


class SliderDP extends React.Component{
    render(){

        const {titulo, productosSDP, dataJson} = this.props;

        const {dataDPSliderImg} = this.props;

        const productos = dataDPSliderImg.map((datos,i)=>{
            return(
                <a key={datos.id} href="/producto/67784/iphone-11-64-gb/" className="cuadroProdDetalle slick-slide slick-cloned" data-scarabitem="67784" tabIndex="-1" data-slick-index="-1" aria-hidden="true">
                    <div className="cuadroImgDetalle contImgProd"><img src={datos.img}/>{/* <span>{datos.title}</span> */}</div>
                </a>
            );
        }) 

        var settings = {
            speed: 500 ,
            slidesToShow: 5,
            slidesToScroll: 5,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            responsive: [
              {
                breakpoint: 780,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }
            ]
          };


        return(
            <section className="slidersDetalleProducto">
                        <div className="productosdetalle">
                            <h2>{titulo}</h2>
                           
                            <div id="carruselAlsoBougth"></div>
                            <div id="carruselAlsoBougthb" className="rowCuadrosProdDetalle slick-initialized slick-slider">
                            <div className="slick-list draggable">
                                <div className="slick-track" >

                                    <Slider {...settings}>
                                        {productos}
                                    </Slider>

                                </div>
                            </div>
                            </div>
                        </div>
                    </section>
        );
    }
}

export default SliderDP;