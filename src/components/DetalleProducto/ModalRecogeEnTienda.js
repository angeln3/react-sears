import React from 'react';
import Fragment from 'render-fragment';

//Redux
import { connect } from 'react-redux';

class ModalRecogeEnTienda extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            colorSeleccionado: ""
        }
    }

    cierraModal2 = () =>{
        console.log('se dio click en modal trasero');
        document.getElementById('modalTienda').style.display = 'none';
        document.getElementById('modalC2').style.display = 'none'; 
    }

    cambioChange2 = (e) =>{
        this.setState({colorSeleccionado: e.target.value});

    }

    componentWillReceiveProps(prop){
        this.setState({colorSeleccionado: prop.datosDP_colorDef});
        this.setState({colorSeleccionado: prop.cambioCOL});
    }
     
    render(){

        const {
            datosDP_CyT, 
            cambioCOL,
            dataDP,
            dataDPImg
        } = this.props;

        return(
            <Fragment>
                <div id="modalTienda">
                <div className="ventanaInfo2">
                        <div className="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened" tabindex="-1" style={{width: '430px', height: 'auto', position: 'absolute',  opacity: '1', overflow: 'visible', }}>
                            <div className="fancybox-skin" style={{padding: '15px', width: 'auto', height: 'auto'}}>
                                <div className="fancybox-outer">
                                    <div className="fancybox-inner" style={{overflow: 'auto', width: '400px', height: '500px'}}>
                                    <div id="modal-click-recoge" style={{display: 'block'}}>
                                        <div className="ico-click-recoge bordeBottom">
                                            <h2>Recoge en Tienda</h2>
                                        </div>
                                        <article className="Box__product">
                                            <div className="fila_producto">
                                                <div className="box__detalleClick">

                                                {dataDPImg.map((datos,i)=>{
                                                    if(datos.order == 1){
                                                        return(
                                                            <img key={i} src={datos.link} alt={dataDP.title} className="tienda_Detalle"/>
                                                        );
                                                    }
                                                })}
                                                
                                                </div>
                                            
                                                <div className="caja__click">
                                                <p className="producto descProducto">{dataDP.title}</p>
                                                <div className="info">
                                                    <p>Vendido por: Sanborns</p>
                                                    <p className="cajaClick">$ {dataDP.sale_price}</p>
                                                </div>
                                                </div>
                                                
                                                <div className="opciones__Talla"></div>
                                                
                                            </div>
                                        </article>


                                    <div class="opciones__Talla">
                                        <div class="boxColor__Talla ">
                                            <span>Color</span>
                                            <div class="opc_colors">
                                                <select id="colorCR1" name="colorCR"  required="required" onChange={this.cambioChange2}>
                                                            {datosDP_CyT.map((datos,i)=>{
                                                                return(
                                                                    <option key={i} value={datos.color}>{datos.color}</option>
                                                                );
                                                            })}
                                                    </select>
                                                    </div>
                                                </div>

                                                <div class="boxTallas__Talla">
                                                    <span>Tallas</span>
                                                    <div class="opc_colors">
                                                        <select  required="required" name="tallaCR" id="tallaCR1">
                                                        {
                                                            (datosDP_CyT.map((tal) => {
                                                                return (tal.sizes.map((ta, it) => {
                                                                    return (
                                                                        <Fragment key={it}>
                                                                            {(tal.color === this.state.colorSeleccionado)
                                                                                ?
                                                                                <option key={it} value={tal.sizes[it].size} selected={this.state.bandera}> {tal.sizes[it].size}</option>
                                                                                : null
                                                                            }
                                                                        </Fragment>
                                                                    );
                                                                }))
                                                            }))}
                                                            </select>
                                                        </div>
                                                </div>

                                        </div>
                                        
                                        
                                        <div className="content__Padres">
                                            <div className="cont-search-tienda">
                                                <div>
                                                <p>Selecciona tu tienda por:</p>
                                                </div>
                                                <div className="display-inline2">
                                                <section>
                                                    <div className="search display-inline">
                                                        <div style={{display: 'none'}}><a id="lnkMuestraSucursales" href="#!" className="link" onclick="ModuleCaja.ClickRecoge.getTiendasByProducto('115842','https://www.sanborns.com.mx/carrito/click-recoge')">Ver Tiendas</a></div>
                                                    </div>
                                                </section>
                                                </div>
                                            
                                                <div id="pnlTiendas" className="tiendasSearch display-inline2" >
                                                <section>
                                                    <p id="txtNombre" className="conttxt">Nombre de Sucursal</p>
                                                    <select id="cboSucursalesCR" onchange="filtraSucursal()" data-placeholder="Ver todas" className="chosen-select Desktop" style={{display: 'none'}}>
                                                        <option value="">Ver todas las tiendas</option>
                                                        <option value="33">LINDAVISTA</option>
                                                        <option value="66">UNIVERSIDAD</option>
                                                        <option value="88">SATELITE</option>
                                                        <option value="68">VILLA COAPA</option>
                                                        <option value="59">SANTA FE</option>
                                                        <option value="49">PLAZA CARSO</option>
                                                        <option value="55">REFORMA 222</option>
                                                    </select>
                                                    <div className="chosen-container chosen-container-single chosen-with-drop" style={{width:'100%'}} title="" id="cboSucursalesCR_chosen">
                                                        <a className="chosen-single">
                                                            <span>LINDAVISTA</span>
                                                            <div><b></b></div>
                                                        </a>
                                                        <div className="chosen-drop">
                                                            <div className="chosen-search"><input type="text" autocomplete="off"/></div>
                                                            <ul className="chosen-results">
                                                            <li className="active-result" data-option-array-index="0" >Ver todas las tiendas</li>
                                                            <li className="active-result result-selected highlighted" data-option-array-index="1" >LINDAVISTA</li>
                                                            <li className="active-result highlighted" data-option-array-index="2" >UNIVERSIDAD</li>
                                                            <li className="active-result" data-option-array-index="3">SATELITE</li>
                                                            <li className="active-result" data-option-array-index="4" >VILLA COAPA</li>
                                                            <li className="active-result" data-option-array-index="5" >SANTA FE</li>
                                                            <li className="active-result result-selected" data-option-array-index="6" >PLAZA CARSO</li>
                                                            <li className="active-result" data-option-array-index="7" >REFORMA 222</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                                
                                            </div>
                                            <div id="contenedorTiendasCR" className="padre1">
                                                <article className="box__distancia" id="artSucursal33" style={{display: 'block'}}>
                                                <div className="contInfo izqFlotante">
                                                    <ul>
                                                        <li className="titTienda">SANBORNS LINDAVISTA</li>
                                                    </ul>
                                                </div>
                                                <p className="descTienda">Av. Montevideo No. 313 Lindavista Gustavo A. Madero Ciudad de México CP. 07300</p>
                                                <p className="Horarios__Tienda">Lunes a Domingo de 11:00 a 21:00 hrs.</p>
                                                <a href="tel:55 55861100" className="telTienda">55 55861100</a><a href="https://www.sanborns.com.mx/carrito/click-recoge/caja/115842/33/" className="Tienda">Recoge aquí</a>
                                                </article>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <a title="Close" className="fancybox-item fancybox-close" onClick={this.cierraModal2}></a>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <div className="" id="modalC2" onClick={this.cierraModal2}></div>
            </Fragment>
        );
    }

}

function mapStateToProps(state){
    return {
        cambioCOL: state.cambioCol.cambioCOLOR
    }
}

//aqui lo conectamos
export default connect(mapStateToProps)(ModalRecogeEnTienda);
