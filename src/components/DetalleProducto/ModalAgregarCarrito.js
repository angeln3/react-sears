import React from 'react';
import Fragment from 'render-fragment';

class ModalCarrito extends React.Component{

    cerrarModal =() => {
        document.querySelector('#modalC').style.display='none';
        document.querySelector('#modalCarro').style.display='none';
    }

    render(){
        return(
            <Fragment>
           <div id="modalCarro">
                <div className="ventanaInfo">
                    <div className="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened" tabIndex="-1" style={{opacity: '1', overflow: 'visible',background:'#fff', zIndex: '10000'}}>
                    <div className="fancybox-skin" style={{padding: '15px', width: 'auto', height: 'auto'}}>
                        <div className="fancybox-outer">
                            <div className="fancybox-inner">
                            <div id="modal-carrito" style={{display: 'block'}}>
                                <div className="check"><span className="ico-check"></span></div>
                                <h2>Se han añadido <span id="anadidos"></span> artículos a tu carrito. Ahora tienes <strong id="cart_count2">3</strong> artículos en tu Carrito.</h2>
                                <ul className="popUp-btns">
                                    <li><a href="/carrito/" className="IrCarrito">Ir a carrito</a></li>
                                    <li><a href="#!" className="SeguirComp">Seguir comprando</a></li>
                                </ul>
                                
                            </div>
                            </div>
                        </div>
                        <a title="Close" className="fancybox-item fancybox-close" onClick={this.cerrarModal}></a>
                    </div>
                </div>
            </div>
         </div>
         <div className="" id="modalC" onClick={this.cerrarModal}></div>
         </Fragment>
        );
    }
}

export default ModalCarrito;