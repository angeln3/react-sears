import React from 'react';

import Fragment from 'render-fragment';

//componentes
import Producto from './Producto'
import ContenedorCompra from './ContenedorCompra'
import Descripcion from './Descripcion'
import SliderDP from './SliderDP'
import ModalCariito from './ModalAgregarCarrito'
import ModalRecogeEnTienda from './ModalRecogeEnTienda'

import json from '../../jsons/Departamentos.json';

class MainContainer extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: json.data
        }
    }

    render(){
        //dataDP contiene toda la información nuestro producto
        const {productosSDP,
             dataDP,
             dataDPMig,
             dataDPImg,
             dataDPAttr,
             dataDPSliderImg,
             dataDPTipo,
             dtaDPVid,
             dataDPfpago,
             datosDP_CyT,
             datosDP_colorDef
            } = this.props;

        const {data} = this.state;

        /* const titulo = data.map((datos,i)=>{
            return(
                <h1 style={{padding:'16px 8px 6px'}}>{datos.title}</h1>
            );
        }); */

        return(
            <Fragment>
                <section className="productMainTitle"><div className="container"><h1 style={{padding:'16px 8px 6px'}}>{dataDP.title}</h1></div></section>

                <section className="productMainContainer">
                    <div className="mod__producto">

                        <Producto dataJson={data} 
                        dataDP={dataDP} 
                        dataDPMig={dataDPMig} 
                        dataDPImg={dataDPImg}
                        dtaDPVid={dtaDPVid}
                        />
                    
                        <ContenedorCompra 
                        dataJson={data}  
                        dataDP={dataDP} 
                        dataDPTipo={dataDPTipo}
                        datosDP_CyT={datosDP_CyT}
                        datosDP_colorDef={datosDP_colorDef}
                        />
                        
                        <Descripcion  
                        dataJson={data} 
                        dataDP={dataDP} 
                        dataDPAttr={dataDPAttr} 
                        dataDPfpago={dataDPfpago}/>
                    
                    
                        <SliderDP
                        titulo="También Compraron" 
                        productosSDP={productosSDP} 
                        dataJson={data} 
                        dataDPSliderImg={dataDPSliderImg}/>

                        
                        <br></br>
                    </div>
                </section>
                <ModalCariito />
                
                <ModalRecogeEnTienda 
                datosDP_CyT={datosDP_CyT} 
                dataDPImg={dataDPImg} 
                dataDP={dataDP} 
                datosDP_colorDef={datosDP_colorDef}/>
        </Fragment>
        );
    }

}

export default MainContainer;