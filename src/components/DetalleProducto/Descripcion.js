import React from 'react';

import Fragment from 'render-fragment';

//Redux
import { connect } from 'react-redux';

//Acciones de Redux
import {
    showExistenciaTienda,
    cambioBonton
    } from '../../actions'



class Descripcion extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            showInfo: '',
            clickExistencia: false
        }
    }

    showDescription = (e) =>{
        let Etiqueta = e.target.innerHTML; 

        switch(Etiqueta){
            case Etiqueta:
                this.setState(()=>({showInfo: Etiqueta}));
                break;

            default:
                this.setState(()=>({showInfo: ''}));
        }
        //para que actulize el estado del nommbre de la etiqueta a ''
        this.props.cambioBonton();
        
    }

    //Consulta con skup y skuh
    ConsultarExistenciaEnTienda = (sku_p) =>{
        //this.setState(()=>({clickExistencia: true}));
        
        this.props.showExistenciaTienda(sku_p);
        
    } 

    componentWillReceiveProps(prop){
        //para evitar que al dale click a existencia en tienda se vaya a descripción
        if(prop.NameEtic === 'Existencia en Tiendas'){
            this.setState(()=>({showInfo: prop.NameEtic}));
        }
        
    } 



    render(){
        //showInfo === 'Descripción' ? 'laDescrip select' : 'laDescrip' && showInfo ==='' ? 'laDescrip select' : 'laDescrip'
        const {showInfo, clickExistencia} = this.state;

        const {
            dataJson, 
            dataDP, 
            dataDPAttr, 
            existenciaTienda,
            dataDPfpago
        } = this.props;


        //existenciaTienda.length == 0 || typeof(existenciaTienda) === "undefined"

        const tiendas = typeof(existenciaTienda) === "undefined" ? <h1>No hay existencia</h1> : 
        existenciaTienda.map((datos,i)=>{
            const substores = Object.values(datos.stores);
            return(
                <Fragment key={i}>
                    <dt>
                        <h5>{datos.state}</h5>
                        <span>1</span>
                    </dt>
                    {substores.map((datos2,i)=>{
                            return (
                                <dd key={i+1}><strong>Direccion: </strong> {datos2.name} {datos2.address.street} {datos2.address.suburb} CP. {datos2.address.cp} </dd>
                            )
                            })
                        }
                </Fragment>
            );
        })

        return(
            <div className="mod__DescripcionP">
                        <a name="describe"></a>
                        <div className="contSucursales">
                            <ul className="viewMenu">
                            <li onClick={this.showDescription} className={showInfo === 'Descripción' ? 'laDescrip select' : showInfo ==='' ? 'laDescrip select' : 'laDescrip'}><span>Descripción</span></li>
                            <li onClick={this.showDescription} className={showInfo === 'Especificación' ? 'select' : ''} ><span>Especificación</span></li>
                            
                            <li onClick={this.showDescription} className={showInfo === 'Formas de Pago' ? 'anchorFormas select' : 'anchorFormas'}><span>Formas de Pago</span></li>
                            {dataDP.stock == 0 || dataDP.status == false ? '' :
                                <li onClick={(e)=> {this.showDescription(e); this.ConsultarExistenciaEnTienda(dataDP.id)}} id="btnProdSucursales" className={showInfo === 'Existencia en Tiendas' ? 'prodSucursales select' : 'prodSucursales'} data-id="67789" ><span>Existencia en Tiendas</span></li>
                            }
                            </ul>
                            <ul className="viewDescrip">
                                <li className={showInfo === 'Descripción' ? 'laDescrip textRaw show' : showInfo === '' ? 'laDescrip textRaw show' : 'laDescrip textRaw'}>
                                   {dataDP.description}
                                    <div id="flix-inpage">
                                        <script type="text/javascript" src="https://media.flixcar.com/delivery/js/inpage/5584/mx/ean/2005681879943?&amp;=5584&amp;=mx&amp;ean=2005681879943&amp;brand=APPLE&amp;ssl=1&amp;ext=.js" async="" crossOrigin="true"></script>
                                        <div id="flixinpage_1577401053360"></div>
                                        <div id="flixinpage_1577401053554"></div>
                                    </div>
                                </li>
                                <li className={showInfo === 'Especificación' ? 'laDescrip show' : 'laDescrip'}>
                                    <dl className="descTable">
                                        {dataDPAttr.map((datos,i)=>{
                                            return (
                                                <Fragment key={i}>
                                                    <dt>{datos.title}</dt>
                                                    <dd>{datos.value}</dd>
                                                </Fragment>
                                            );
                                        })}
                                        
                                    </dl>
                                </li>
                                <li className={showInfo === 'Formas de Pago' ? 'anchorFormasCont show' : 'anchorFormasCont'}>
                            
                                        {/* dataDPfpago.map((datos,i)=>{
                                            return (
                                                <div key={i} className="descformaspago">
                                                        <div className="descimg"><img src={datos.img}/></div>
                                                 </div>
                                            );
                                        })  LO COMENTAMOS POR QUE EL SERVICIO DE POLO LAS FORMAS DE PAGO ESTAN DIFERENTES*/ }

                                </li>
                                
                                {dataDP.stock == 0 || dataDP.status == false ? '' :
                                    <li id="ctnProdSucursales" className={showInfo === 'Existencia en Tiendas' ? 'prodSucursales show' : 'prodSucursales'}>
                                    <ul>
                                        <dl className="resultadoSucursal">
                                            <div className="titleContent">
                                                <p className="titleTienda">Tiendas</p>
                                                <p className="titleCant">Existencia</p>
                                            </div>
                                            {tiendas}
                                        </dl>
                                        </ul>
                                    </li>
                                }
                                
                            </ul>
                        </div>
                    </div>
        );
    }
}

function mapStateToProps(state){
    return {
        existenciaTienda: state.data.list12,
        NameEtic: state.nombreETIC
    }
}

export default connect(mapStateToProps,{showExistenciaTienda, cambioBonton})(Descripcion);