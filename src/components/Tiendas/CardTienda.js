import React from 'react';

class CardTienda extends React.Component{

    constructor(props){
        super(props);
        this.state ={
            latitud: 19.4411227,
            longitud: -99.2045768
        }
    }

    Coordenadas = () =>{
        

        var Latitud, Longitud;

        
        navigator.geolocation.getCurrentPosition(
            function(cor){
                Latitud = cor.coords.latitude;
                Longitud = cor.coords.longitude;
                localStorage.setItem("LatitudLE", Latitud)
                localStorage.setItem("LongitudLE", Longitud)
            },
            function(error) {
				var errores = {1: 'Permiso denegado', 2: 'Posición no disponible', 3: 'Expiró el tiempo de respuesta'};
				console.log("Error: " + errores[error.code]);
			},
			{
				enableHighAccuracy: true,
				maximumAge:  5000,
				timeout: 10000
			}
        );
        
        var LongitudLE = localStorage.getItem("LongitudLE")
        var LatitudLE = localStorage.getItem("LatitudLE")

        Longitud = localStorage.getItem("LongitudLE")
        Latitud = localStorage.getItem("LatitudLE")


        if(LatitudLE == null && LongitudLE == null){
            this.setState({
                latitud: 19.4411227,
                longitud: -99.2045768
            });
        }else{
            this.setState({
                latitud: LatitudLE,
                longitud: LongitudLE
            });
        }

    }

    componentDidMount(){

        this.Coordenadas();

        document.getElementById("activarGeo").addEventListener("click", function(){

            var Latitud, Longitud;
            
            navigator.geolocation.getCurrentPosition(
                function(cor){
                    Latitud = cor.coords.latitude;
                    Longitud = cor.coords.longitude;
                    localStorage.setItem("LatitudLE", Latitud)
                    localStorage.setItem("LongitudLE", Longitud)
                },
                function(error) {
                    var errores = {1: 'Permiso denegado', 2: 'Posición no disponible', 3: 'Expiró el tiempo de respuesta'};
                    console.log("Error: " + errores[error.code]);
                },
                {
                    enableHighAccuracy: true,
                    maximumAge:  5000,
                    timeout: 10000
                }
            );
            
            var LongitudLE = localStorage.getItem("LongitudLE")
            var LatitudLE = localStorage.getItem("LatitudLE")

            Longitud = localStorage.getItem("LongitudLE");
            Latitud = localStorage.getItem("LatitudLE");
        
            if(LatitudLE == null && LongitudLE == null){
                LatitudLE = 19.4411227
                LongitudLE = -99.2045768
                console.log("LATUTUD",LatitudLE, "LONGITUD",LongitudLE);
            }else{
                console.log("LATUTUD",LatitudLE, "LONGITUD",LongitudLE);
            }
        });
    }

    render(){

        const { 
            datos
        } = this.props;

        const{
            latitud,
            longitud
        } = this.state;

        const ubicacion = "https://www.google.com.mx/maps/dir/"+latitud+","+longitud+"/"+datos.direccion+"";

        return(
            <div data-id={datos} data-tienda={datos.tienda} data-edo={datos.estado} className="card" >
                <dl className="SucDir">
                    <dt>{datos.sucursal}</dt>
                        <dd>{datos.direccion}
                            <a href={ubicacion} target="_blank" rel="noopener noreferer" className="linkDir">Ir a tienda</a>
                        </dd>
                    </dl>
                    <dl className="SucLada">
                        <dt>Lada</dt>
                        <dd>{datos.lada}</dd>
                    </dl>
                    <dl className="SucTel">
                        <dt>Teléfonos</dt>
                        <dd>
                            <a href={'tel:'+datos.telefono+''} className="ico-tel">{datos.telefono}</a>
                        </dd>
                    </dl>
            </div>
        );
    }

}

export default CardTienda;
