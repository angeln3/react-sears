import React from 'react';

class Enacabezado extends React.Component{
    render(){
        return(
            <section className="sec__Internas">
                    <div className="container">
                        <div className="breadcrumb">
                            <ul>
                                <li>
                                    <a href="index.html">Inicio</a>
                                </li>
                                <li>
                                    <a href="./">Inicio</a>
                                </li>
                                <li>
                                    <a href="#">Directorio de tiendas</a>
                                </li>
                            </ul>
                        </div>
                        <h1>Directorio de tiendas</h1>
                        <h2>Localiza tu tienda Sanborns o Sanborns Café mas cercano.</h2>
                        <h3 className="small">* Para mostrarte la mejor ruta necesitamos tu localización, en caso de no permitirnos usarla la ubicación por default sera Plaza Carso 
                             <span className="activarGeo" id="activarGeo"> Activar Geolocalización </span>
                        </h3>
                    </div>
                    
                </section>
        );
    }
}

export default Enacabezado;