import React from 'react';


import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

//stilos
import '../css/styles.css';


import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
//import logo from './logo.svg';
//import '../sass/includes/modulos/header.sass';

//componentes
import Footer from './footer/Footer';  
import Header from './Header/Header';

//componentes pagina
import Home from '../pages/Home';
import DetalleProducto from '../pages/DetalleProducto';
import Tiendas from '../pages/Tiendas';

//Redux
import { connect } from 'react-redux';
//Acciones de Redux
import { 
    showLogos, 
    showFooter, 
    showFooter2, 
    showCategorias
 } from '../actions'

class Index extends React.Component {

    componentWillMount(){
        this.props.showLogos();
        this.props.showFooter();
        this.props.showFooter2();
        this.props.showCategorias();
    } 

  render(){  

    const { 
        logos, 
        infoFooter,
        infoFooter2,
        categorias

    } = this.props;


    return (
      <div className="App full fixHeader wAppM indexDemo"> 
      
        <Router>
        
          <Header categorias={categorias}></Header>
          
          <Switch>

            <Route exact path='/' component={Home}/>
            <Route exact path='/producto/:id' component={DetalleProducto}/> 
            <Route exact path='/Tiendas' component={Tiendas} />

          </Switch>
              
          <Footer logos={logos} infoFooter={infoFooter} infoFooter2={infoFooter2}></Footer>

        </Router>
        
      </div>
    ); 
  }
}

function mapStateToProps(state){
    return{
        logos: state.data.list,
        infoFooter: state.data.list2,
        infoFooter2: state.data.list3,
        categorias: state.data.list9 
    }
}

export default connect(mapStateToProps, 
    {
      showLogos, 
      showFooter, 
      showFooter2,
      showCategorias
    
    })(Index)
