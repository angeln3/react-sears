import React from 'react';

import Fragment from 'render-fragment';


class MenuMovil extends React.Component {

     constructor(props){
        super(props);
        this.state={
           showMenuMovil: false,
           subMenuMovil: false,
           subMenuMovilE: false,
           nombreEtiqueta: ''

        }
    }

    /* componentWillReceiveProps(){
        this.setState(()=>({showMenuMovil: this.props.showMenu}))
    } */

    
    showSubMenuMovil = () =>{
        this.setState((state)=>({subMenuMovil: !state.subMenuMovil}))
        //console.log('se dio click en ver categorias',e.target);
    }

    subMenuMovilE = () =>{
        this.setState((state)=>({subMenuMovilE: !state.subMenuMovilE}))
    }
    

    showSubSubMenus =(e) =>{
        
        let Etiqueta = e.target.innerHTML;
        switch(Etiqueta){
            case Etiqueta:
                this.setState(()=>({nombreEtiqueta: Etiqueta}))
                document.getElementById(Etiqueta).classList.toggle("show");
                break;
            default:
                this.setState(()=>({nombreEtiqueta: ''}))
        }
    }
    /*
    componentDidMount(){
 
        const url = "https://api.myjson.com/bins/bwybg";
        fetch(url)
        .then(res => {return res.json()})
        .then(datos => this.setState({ data: datos.data.content}))
        .catch(()=> console.log("Can't access " + url + " response. Blocked by browser?"))
    } */

  render(){  

    //const{data}= this.state;
    const {showMenuMovil, subMenuMovil, subMenuMovilE, nombreEtiqueta} = this.state;


    const {categorias} = this.props;

    const categoriasM = categorias.map((datos,i)=>{
      
        const child = Object.values(datos.child);

        /* showSubSubMenus =(e) =>{
            let Etiqueta = e.target.innerHTML;
            switch(Etiqueta){
                case datos.name:
                    this.setState(()=>({nombreEtiqueta: datos.name}))
                    break;            
            }
        } */
        //showInfo === 'Descripción' ? 'laDescrip select' : 'laDescrip' && showInfo ==='' ? 'laDescrip select' : 'laDescrip'
        return(
        <Fragment key={i}>
            <dt onClick={this.showSubSubMenus} id={datos.name} className={nombreEtiqueta === datos.name ? 'show' : ''}>{datos.name}</dt>
                    <dd>    
                         <ul>
                        {child.map((datos2,i)=>{
                            return (
                                    <li key={i+1}>
                                        <a href="#">{datos2.name}</a>
                                    </li>
                            );
                            })}
                        </ul>
                    </dd>
        </Fragment>    
        );
    });  


    return (

        //{showMenuMovil ? 'header__movil view' : 'header__movil'} -> con props pero se ejecuta despues de dos clicks
        <section className='header__movil'>

            <nav className="navegacionM">

                <dl className={subMenuMovil ? 'navProductos show' : 'navProductos'}>
                    
                    <dt onClick={this.showSubMenuMovil}>Ver Categorias</dt>
                    <dd>
                        <dl>
                            {categoriasM}
                        </dl>
                    </dd>
                </dl>
                <dl className={subMenuMovilE ? 'navRecomendados show' : 'navRecomendados'}>
                    <dt onClick={this.subMenuMovilE}>Especiales</dt>
                    <dd>
                        
                        <dl>
                        <dt><a href="http://facturaelectronica.sanborns.com.mx/" target="_blank">Factura Electrónica</a></dt>
                        </dl>
                        <dl>
                        <dt><a href="/categoria/1007/gadgets/">Gadgets</a></dt>
                        </dl>
                        <dl>
                        <dt><a href="/categoria/12/videojuegos/">Gamers</a></dt>
                        </dl>
                        <dl>
                        <dt><a href="/categoria/1003/fotografia/">Photo</a></dt>
                        </dl>
                        <dl>
                        <dt><a href="/categoria/501/libros/">Libros</a></dt>
                        <dt><a href="/categoria/50202/digitales/">Revistas Digitales</a></dt>
                        </dl>
                        <dl>
                        <dt><a href="/Directorio_tiendas/">Directorio de tiendas</a></dt>
                        </dl>
                    </dd>
                </dl>
            </nav>
        </section>
      ); 
    }
  }
  
  export default MenuMovil;
  