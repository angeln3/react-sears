import React from 'react';

//jquery
//import $ from 'jquery';

//json
//import data from '../../jsons/Departamentos';

import {Link} from 'react-router-dom';

class HeaderInferior extends React.Component{

  constructor(props){
    super(props);
    this.state ={
      isClick: false
    }
  }

  componentDidMount(){ 

    /* $('.productosNewCat').click(function(){
      $('.menuHeader').toggle();
    }); */
   /*  //fetch('http://clauluna.com/CLARO/api/getHomeSears')
      //.then(res=>res.json())
      //.then(datos => this.setState=({datos:datos.data}))
    const url = "https://api.myjson.com/bins/bwybg";
    fetch(url)
    .then(res=>{ return res.json() })
    .then(datos=> this.setState({ data: datos.data.content }))
    .catch(()=> console.log("Can't access " + url + " response. Blocked by browser?"))
 */
  } 

  HandlerClickMenu = () => {
    this.setState(state=>({isClick: !state.isClick}))
  }

    render(){

      const {categorias} = this.props;

      //data.map
      const tituloC = categorias.map( (datos,i) => {
        const child = Object.values(datos.child);
        return(
          <dl key={i}>
          <dt>
          <a href="/categoria/10/tecnologia-y-electronica/" className={datos.class}>{datos.name}</a>
          <dl>
            <dt>
                <dl>
                {child.map((datos2,i)=>{
                  const subc = Object.values(datos2.subcatego); 
                         return(
                                  <dt key={i+2} className="more">
                                        <a href="/categoria/1001/audio/"> 
                                        {datos2.name}
                                        </a>
                                        <ul>
                                          {subc.map((datos3,i)=>{
                                             return(
                                                  <li key={i+3} className="alone"><a href="/categoria/100102/audifonos/" >
                                                    {datos3.name}
                                                    </a>
                                                  </li>
                                            )
                                          })

                                          }
                                          
                                        </ul>
                                    </dt>

                                    )
                                  })      

                                }
                              
                            </dl>
                        </dt>
                      </dl>
                  </dt>
                  </dl>
                )
        });          

        const {isClick} = this.state;
            
        return(

            <div className="header__inferior">
                    <div className="container">
                        <div className="box__mobile">
                            <div className="nav__mobile"></div>
                        </div>

                        <nav className="productosHead"  onClick={ this.HandlerClickMenu}>

                            <ul>
                                <li className={isClick ? 'productosNewCat' : 'productosNewCat hide' }><a href="#">Ver Categorias</a>

                                    <div className="menuHeader" style={{display: isClick ?  'block' : 'none' }}>
                                      
                                        <div className="subMenu">
                                           {tituloC}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                        <nav className="promoHeader">
                            <ul>
                                <li className="alone"><a href="https://facturaelectronica.sanborns.com.mx/" target="_blank">Factura Electrónica</a></li>
                                <li className="alone"><a href="/categoria/1007/gadgets/">Gadgets</a></li>
                                <li className="alone"><a href="/categoria/12/videojuegos/">Gamers</a></li>
                                <li className="alone"><a href="/categoria/1003/fotografia/">Photo</a></li>
                                <li className="alone"><a href="/categoria/501/libros/">Libros</a></li>
                            </ul>
                        </nav>
                        <nav className="menuTiendas noSubMenu">
                            <ul>
                                <li className="urlExt alone" style={{width: '50%'}}><a href="/categoria/50202/digitales/" rel="follow" target="_blank">Revistas Digitales</a></li>
                                <li className="urlExt alone" style={{width: '50%'}}><p rel="follow" className="dirTienda"><Link to="/Tiendas">Ir a tienda</Link></p></li>
                            </ul>
                        </nav>
                    </div>
                </div>

        );
    }
}

export default HeaderInferior;