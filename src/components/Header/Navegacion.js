import React from 'react';
import {Link} from 'react-router-dom';

class Navegacion extends React.Component{

    constructor(){
        super();
        this.state={
            ayuda: false,
            login: false,
            carrito: false,
            nombreEtiqueta: ''
        }
    }

    componentDidMount(){

        var idAyuda = document.getElementById('obtenAyuda');
        idAyuda.addEventListener("mouseover", function() { this.classList.add('log') });
        idAyuda.addEventListener("mouseout", function() { this.classList.remove('log') });

        var idRegistro = document.getElementById('registroBienbenido');
        idRegistro.addEventListener("mouseover", function() { this.classList.add('log') });
        idRegistro.addEventListener("mouseout", function() { this.classList.remove('log') });

        var idCarrito = document.getElementById('cantidadProductos');
        idCarrito.addEventListener("mouseover", function() { this.classList.add('log') });
        idCarrito.addEventListener("mouseout", function() { this.classList.remove('log') });

    }


    showAyuda = () =>{
        document.getElementById('obtenAyuda').classList.toggle('log');
    }

    showLogin = () =>{
        document.getElementById('registroBienbenido').classList.toggle('log');
    }

    showCarrito = () =>{
        document.getElementById('cantidadProductos').classList.toggle('log');
    }


    /* showMenus =(e) =>{
        
        let Etiqueta = e.target.innerHTML;
        switch(Etiqueta){
            case Etiqueta:
                this.setState(()=>({nombreEtiqueta: Etiqueta}))
                break;
            default:
                this.setState(()=>({nombreEtiqueta: ''}))
        }
    } */

    render(){


        const {ayuda,login,carrito,nombreEtiqueta} = this.state;

        return(
            <nav className="menuPrincipal menuAnim">
                            <ul>
                                <li className="buscar" ><a href="#">Buscar</a></li>
                                <li className={nombreEtiqueta === '<span>Obtén</span> Ayuda' ? 'ayuda log' : 'ayuda'} onClick={this.showAyuda} id="obtenAyuda" ><a href="#" onClick={this.showMenus}><span>Obtén</span> Ayuda</a>
                                    <div className="box__ayuda">
                                        <dl><dt className="icoLlam">Llámanos</dt>
                                            <dd><a href="tel:018007262676">800-726-2676</a> Dentro de la República Mexicana. </dd>
                                        </dl>
                                        <dl><dt className="icoCha"><span className="btn-chatZ">Chat</span></dt></dl>
                                        <dl><dt className="icoEma"><a href="mailto:sanborns@sanborns.com.mx">Escríbenos</a></dt></dl>
                                    </div>
                                </li>
                                <li className={nombreEtiqueta === '<span>Bienvenido</span>Regístrate' ? 'login log' : 'login'} id="registroBienbenido" onClick={this.showLogin}><a href="#" id="userInformation" onClick={this.showMenus}><span>Bienvenido</span>Regístrate
                                                      </a>
                                    <div className="box__login" id="box__login">
                                        <dl className="logIn"><dt><Link to="/DetalleProducto" className="redBtn btn-login">Ingresar</Link></dt>
                                            <dd>
                                                <p>¿Eres Nuevo?</p><a className="standardBtn btn-registro" href="/loginregistro?redirect=https://www.sanborns.com.mx/?gclid=Cj0KCQiAq97uBRCwARIsADTziybMhqL1I2WwMqVk7xuUFLJ420UmT5TEq42vD6jR3qGsF_bIejjMikAaAuVUEALw_wcB#">Regístrate</a></dd>
                                        </dl>
                                    </div>
                                </li>
                                <li className="wishlist notification" id="wishlist_snippet"><span className="push">0</span><a href="/mi-cuenta/listadeseos"><span>Lista de</span> Deseos</a>
                                    <input type="hidden" id="idsProducto" name="idsProducto" />
                                    <input type="hidden" id="logeado" name="logeado" value="0" />
                                    <input type="hidden" id="idsProducto" name="idsProducto" />
                                </li>
                                <li className={nombreEtiqueta === '<span>Ingresar</span> Carrito' ? 'cart notification cartPreview log' : 'cart notification cartPreview'}  id="cantidadProductos" onClick={this.showCarrito}><span className="push" id="cart_count">0</span><a href="#" onClick={this.showMenus}><span>Ingresar</span> Carrito</a>
                                    <div className="box__carrito">
                                        <h3>Mi Carrito</h3>
                                        <div className="boxCarritoCont">
                                            <article className="caja__product">
                                                <div className="fila_producto">
                                                    <div className="bcp_producto">
                                                        <div className="contImg"></div>
                                                        <div className="contInfo">
                                                            <a href="#">
                                                                <p className="producto" id="carrtPrewTitle">Aún no tienes productos en tu carrito.</p>
                                                            </a>
                                                            <div className="info">
                                                                <p id="carrtPrewEmbarcado"></p>
                                                                <p id="carrtPrewEntrega"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        {/* <div className="botones_cajaPago">
                                            <div className="botones"><a href="#" className="botonComprarClick">Ir al carrito</a><a href="#" id="botonComprarPreview" className="botonComprarAhora">Comprar Ahora</a></div>
                                        </div> */}
                                    </div>
                                </li>
                            </ul>
                        </nav>
        );
    } 

}

export default Navegacion;