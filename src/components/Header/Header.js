import React from 'react';
//componentes
import Buscador from './Buscador';
import Navegacion from './Navegacion';
import HeaderInferior from './HeaderInferior';
import MenuMovil from './menuMovil';
//import '../sass/includes/modulos/header.sass';
//import logo_sears from '../https://www.sanborns.com.mx/img/logo_sears.png';
import {Link} from 'react-router-dom';
//css
import '../../css/Header/Header.css';


class Header extends React.Component {

    constructor(props){
        super(props);
        this.state={
           showMenuMovil: false
        }
    }

    ShowMemuMovil = () =>{
        document.querySelector("html").classList.toggle("fixed");
        document.querySelector(".header__movil").classList.toggle("view");
    }


  render(){  

    const {categorias} = this.props;

    return (
          <header className="viewSearch">
                <div className="header__middle">
                    <div className="container">
                        <Link to="/" title="Ir a la Página principal de Sanborns" className="header__logo" />
                        
                        <div className="desktopMenu" onClick={this.ShowMemuMovil}></div>

                        <Buscador/>
                        <Navegacion/>
                        <div id="modal-listadeseos" style={{display: 'none'}}>
                            <div className="check">
                                <span className="ico-check"></span></div>
                            <h2>Se ha añadido un nuevo artículo a tu lista de deseos.</h2>
                            <ul className="compra-btns">
                                <li><a href="/mi-cuenta/listadeseos" className="btn-rojo">Ir a lista de deseos</a></li>
                                <li>
                                    <button className="btn-azul">Seguir Comprando</button>
                                </li>
                            </ul>
                            <div className="productos-relacionados">
                                <div className="container">
                                    <div className="mod-5">
                                        <h2>También te puede interesar</h2>
                                        <div className="productos-relacionados">
                                            <div id="agregarRecomendaciones" className="container">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <HeaderInferior categorias={categorias} />
                <MenuMovil categorias={categorias} showMenu={this.state.showMenuMovil}/>

    </header>
    ); 
  }
}
//showMenu={this.state.showMenuMovil}

export default Header;
