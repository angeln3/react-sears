import React from 'react';
import '../../css/Header/Buscador.css';


class Buscador extends React.Component {
  render(){  
    return (
      <div className="search">
          <section className="navbar-form navbar-left hpadding0 hmargecontenidozul" id="frmBuscador">
              <div className="contSearch suggestSearch">
                  <div className="input2">
                      <input type="hidden" id="selectCategoriasMenu" name="selectCategoriasMenu" />
                      <input type="search" placeholder="¿Qué es lo que buscas?" name="search" id="search" autoComplete="off" className="ui-autocomplete-input" />
                      <button type="button" className="bt__search"></button>
                      <div id="recentSearch">
                          <h5>Búsquedas recientes</h5></div>
                  </div>
                  <div id="suggestions"></div>
              </div>
          </section>
      </div>
      ); 
    }
  }
  
  export default Buscador;
  