import React from 'react';

//import datosjson from '../jsons/Productos';

//componentes
import Compartir from './Compartir'
import AgregarCarrito from './AgregarCarrito'

import Slider from 'react-slick';
//Redux




class SliderView2 extends React.Component {

  /*   constructor(){
        super();
        this.state={
            data: []
        }
    }

componentDidMount(){
    //fetch('http://clauluna.com/CLARO/api/getHomeSears')
        //.then(res=>res.json())
        //.then(datos => this.setState=({datos:datos.data}))
    const url = "https://api.myjson.com/bins/12d364";
    fetch(url)
    .then(res=>{ return res.json() })
    .then(datos=> this.setState({ data: datos.data.content }))
    .catch(()=> console.log("Can't access " + url + " response. Blocked by browser?"))

    }      
 */

  render(){  
    // constante que devuelve cada articulo
    //const {data} = this.state;

    const {productosCarrusel, titulo, clase} = this.props;

    //data.map
    const productos = productosCarrusel.map((datos,i)=>{

        //funcion para poner coma a miles
        const coma =(numero)=>{
            
                numero = numero.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                numero = numero.split('').reverse().join('').replace(/^[\,]/,'');
                //const arr = numero.toString().match(/.{1,4}/g);
                return numero
           
        }

        const sinDescuento = ()=>{
            
            //funcion para saber si un producto tiene descuento
            if(datos.discount > 0){
                return(
                    <div className="info">
                        <div className="infoDesc"><span className="precioant">$ {coma(datos.price)}</span>       
                            <div className="descText"><span>-{datos.discount}%</span></div>
                        </div><span className="preciodesc">${coma(datos.sale_price)}</span>
                        <div className="infoMensualidades"></div>
                    </div>
                );

            }else{
                return(
                    <div className="info">
                        <div className="infoDesc"><span className="precioant"></span>       
                        </div><span className="preciodesc">${coma(datos.sale_price)}</span>
                        <div className="infoMensualidades"></div>
                    </div>
                );
                
            }
        } 

        return(
            <article key={i} className="productbox slick-slide slick-cloned" id="promocion117802"  tabIndex="0" data-slick-index="0" aria-hidden="false">
                <div className="vistaRapida">
                    <div className="contImgProd"><img src={datos.img} alt={datos.title} title={datos.title}/></div>
                    <a href="/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="linkProducto" tabIndex="0"></a>
                </div>
                <div className="carruselContenido">
                    <a href="/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="descrip" tabIndex="0">
                        <p>{datos.title}
                            <br></br>
                        </p>
                    </a>
                    
                    {sinDescuento()}

                    <div className="sellIcons">
                            
                            <AgregarCarrito/>

                            <Compartir/>
                            
                        </div>
                </div>
            </article> 
        );
    });

    var settings = {
        speed: 1500 ,
        slidesToShow: 5,
        slidesToScroll: 5,
        infinite: true,
        rtl: true,
        responsive: [
          {
            breakpoint: 780,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
        ]
      };

    return (
        <section className="carruselesNewH">
        <h2 className="noPaddTop">{titulo}</h2>
        <div className="rowProductos">
            <div id="carruselTemporada" style={{display: 'none'}}></div>
            <div id="sliderPrincipal" className={clase}>
                <div className="slick-list draggable">
                    <div className="slick-track" style={{opacity: '1'}}>

                    <Slider {...settings}>
                            {productos}
                        </Slider> 
                    
                    </div>
                </div>
            </div>
        </div>
        </section>
    ); 
  }
}



export default SliderView2;
