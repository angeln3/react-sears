import React from 'react';

import Slider from './Slider';

//import datosjson from '../jsons/Productos';



class Footer extends React.Component {

     constructor(){
        super();
        this.state={
            nombreEtiqueta: ''
        }
    }

    /*  componentDidMount(){
        $('#loMasVisto').click(function(){
            $('#carruselMasVisto').css('display', 'block');
            $('#carruselMasVendido').css('display', 'none');
            $('#carruselMasNuevo').css('display', 'none');
            $('#loMasVendido').removeClass('activeLetter');
            $('#loMasNuevo').removeClass('activeLetter');
            $(this).addClass('activeLetter');
        });
        $('#loMasVendido').click(function(){
            $('#carruselMasVisto').css('display', 'none');
            $('#carruselMasVendido').css('display', 'block');
            $('#carruselMasNuevo').css('display', 'none');
            $(this).addClass('activeLetter');
            $('#loMasVisto').removeClass('activeLetter');
            $('#loMasNuevo').removeClass('activeLetter');
        });
        $('#loMasNuevo').click(function(){
            $('#carruselMasVisto').css('display', 'none');
            $('#carruselMasVendido').css('display', 'none');
            $('#carruselMasNuevo').css('display', 'block');
            $(this).addClass('activeLetter');
            $('#loMasVendido').removeClass('activeLetter');
        });
    } */

    showSlider = (e) =>{
        
        let Etiqueta = e.target.innerHTML;
        switch(Etiqueta){
            case Etiqueta:
                this.setState(()=>({nombreEtiqueta: Etiqueta}))
                break;
            default:
                this.setState(()=>({nombreEtiqueta: ''}))
        }
    }
    /*
    componentDidMount(){
            //fetch('http://clauluna.com/CLARO/api/getHomeSears')
            //.then(res=>res.json())
            //.then(datos => this.setState=({datos:datos.data}))
        const url = "https://api.myjson.com/bins/ed3rw";
        fetch(url)
        .then(res=>{ return res.json() })
        .then(data1=> this.setState({ data1: data1.data.content }))
        .catch(()=> console.log("Can't access " + url + " response. Blocked by browser?"))
 
        const url2 = "https://api.myjson.com/bins/12d364";
        fetch(url2)
        .then(res=>{ return res.json() })
        .then(datos=> this.setState({ data2: datos.data.content }))
        .catch(()=> console.log("Can't access " + url + " response. Blocked by browser?"))

        }  */

    render() {
        
        const {productosSlider, productosSlider2} = this.props;
        //const {data1, data2, data3} = this.state;
        const {nombreEtiqueta} = this.state;
        
        return (

            <section className="tabsLoMas">
                <ul className="loMasBtns">
                    <li><a href="#!" className={nombreEtiqueta === 'Lo más visto' ? 'activeLetter' : nombreEtiqueta === '' ? 'activeLetter' : ''} id="Lo más visto" onClick={this.showSlider}>Lo más visto</a></li>
                    <li><a href="#!"  className={nombreEtiqueta === 'Lo más vendido' ? 'activeLetter' : ''} id="Lo más vendido" onClick={this.showSlider}>Lo más vendido</a></li>
                    <li><a href="#!"  className={nombreEtiqueta === 'Lo más nuevo' ? 'activeLetter' :  ''} id="Lo más nuevo" onClick={this.showSlider}>Lo más nuevo</a></li>
                </ul>
                <span className="VerTodoBtn"><a href="/promociones/masvisto/" id="linkmasvistos">Ver todo</a></span>

                    <Slider id="carruselMasVisto" display={{display: nombreEtiqueta === 'Lo más visto' ? 'block' : nombreEtiqueta === '' ? 'block' : 'none'}} clase="slick-initialized slick-slider" productosSlider={productosSlider}></Slider>
                    <Slider id="carruselMasVendido" display={{display: nombreEtiqueta === 'Lo más vendido' ? 'block' : 'none'}} clase="slick-initialized slick-slider" productosSlider={productosSlider2}></Slider>
                    <Slider id="carruselMasNuevo" display={{display: nombreEtiqueta === 'Lo más nuevo' ? 'block' : 'none'}} clase="slick-initialized slick-slider" productosSlider={productosSlider}></Slider>                  
            </section>
            
            ); 
            }
        }
                        
export default Footer;
  