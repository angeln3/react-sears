import React from 'react';

class Compartir extends React.Component{

    render(){
        return(
            <div className="shareIcon">
                <div className="shareMe">
                    <ul>
                        <p>Compartir vía:</p>
                        <li>
                            <a href="http://www.facebook.com/sharer.php?u=https://www.sanborns.com.mx/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="icoFB" target="_blank" tabIndex="0"></a>
                        </li>
                        <li>
                            <a href="http://twitter.com/home?status=Leyendo https://www.sanborns.com.mx/producto/117802/pantalla-sansui-43-smx4319usm-uhd/enClaroshop" className="icoTW" target="_blank" tabIndex="0"></a>
                        </li>
                        <li>
                            <a href="whatsapp://send?text=https://www.sanborns.com.mx/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="icoWS" data-action="share/whatsapp/share" tabIndex="0"></a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Compartir;