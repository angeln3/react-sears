import React from 'react';
import Slider from 'react-slick';

class Footer extends React.Component {

    /* constructor(){
        super();
        this.state={
            datos: []
        }
    }

    componentDidMount(){
        const url = "https://api.myjson.com/bins/17rbgc";
        fetch(url)
        .then(res=> {return res.json()} )
        .then(datos=> this.setState({ datos: datos.data}))
        .catch(() => console.log("Can't access " + url + " response. Blocked by browser?"))
    } */

    render() {

        const {productosSambrons} = this.props;

        const img = productosSambrons.map((datos)=>{
            if(datos.widget==='sliderSamborns'){

                const imagenes = Object.values(datos.object);
                return(

                    imagenes.map((datos)=>{

                        return(
                            <a href={datos.href} target="_blank" className="boxbanner slick-slide" tabIndex="-1"  data-slick-index="0" aria-hidden="true">
                            <img src={datos.img}/>
                            </a>
                        );

                    })
                
                );
            }
        });

        var settings = {
            dots: false,
            slidesToShow: 5,
            rtl: true,
            speed: 1500,
            responsive: [
              {
                breakpoint: 780,
                settings: {
                  dots: false,
                  slidesToShow: 3
                }
              }
            ]
          };

        return (

            <section className="boxbannersFull">
              <div id="banners4" className="rowboxbanners3 slick-initialized slick-slider">
                  
                  <div className="slick-list draggable">
                      <div className="slick-track" style={{opacity: '1'}}>
                          
                          <Slider {...settings} >
                                 {img}

                            </Slider>
                      </div>
                  </div>
                  
              </div>
          </section>
            
            ); 
            }
        }
                        
export default Footer;
  