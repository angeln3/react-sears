import React from 'react';
//slider
import Slider from 'react-slick';


class Carrusel extends React.Component {

  /* constructor(){
    super();

    this.state={
      datos:[]
    };

  }

  componentDidMount(){
    //fetch('http://clauluna.com/CLARO/api/getHomeSears')
      //.then(res=>res.json())
      //.then(datos => this.setState=({datos:datos.data}))

    const url = "https://api.myjson.com/bins/1btnz0";

      fetch(url)
      .then(res => { return res.json() })
      .then(datos => {
            this.setState({ datos: datos.data })
        })
       .catch(() => console.log("Can't access " + url + " response. Blocked by browser?"))
  } */

  render(){  

    const {productosCaruselP}= this.props;

    //datos.map
    const imagenes = productosCaruselP.map((datos,i)=>{

      if(datos.widget==='principalSliderWeb'){

        const imagen = Object.values(datos.object);

        return(
          
            imagen.map((datos2,i)=>{
              return(
                <div>
                  <img key={i} src={datos2.img} />
                </div>
              )     
            })
                 
        );

      }
    });
    // en el slider solo retorna una etiqueta por cada slider <div></div>
    return (
      
      <div style={{width: '100%'}} className="slick-track">  
          <Slider
          speed={1500}
          slidesToShow={1}
          slidesToScroll={1}
          infinite={true}
          autoplay={true}
          autoplaySpeed={5000}
          arrows={true}
          >
            {imagenes}

          </Slider>
      
        
      </div>
  
    ); 
  }
}

export default Carrusel;
