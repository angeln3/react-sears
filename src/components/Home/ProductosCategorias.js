import React from 'react';

import Productos from './Productos'

class Footer extends React.Component {
    render() {

        const {
            productosCat
        } = this.props;
        
        return (

            <section className="productosCateg" style={{position: 'relative'}}>
                <a id="andisco" className="ancla" style={{top: '-300px', position:'absolute'}} ></a>
                <div id="discoveryEm"></div>
                <Productos productosCat={productosCat} />
                <div className="verMas">
                    <div className="verMasBtn" id="verMasDiscov">Ver más</div>
                </div>
            </section>
            
            ); 
            }
        }
                        
export default Footer;
  