import React from 'react';

class AgregarCarrito extends React.Component{

    render(){
        return(
            <div className="actionIcons">
                <ul>
                    <li className="addCart"><a href="#!" className="addCart-animate tooltip tooltip-effect-1" tabIndex="0"><span className="tooltip-content"><p>Agregar a mi carrito</p></span></a></li>
                    <li className="lineHeart bheartCarrusel"><a href="/mi-cuenta/listadeseos" className="blueHeart2 tooltip tooltip-effect-1" style={{visibility: 'hidden'}} tabIndex="0"><span className="tooltip-content"><p>Agregar a lista de deseos</p></span></a></li>
                </ul>
            </div>
        );
    }
}

export default AgregarCarrito;