import React from 'react';

import Fragment from 'render-fragment';

class Productos extends React.Component {

    render() {

        const {
            productosCat
        } = this.props;

        


        const productos = productosCat.map((datos,i)=>{


            const solo5A = productosCat.filter(datos=> datos.length < 5);

            const precioDescuento = () => {
                //console.log("número de articulos", productosCat[1]);
                if(datos.discount > 0){
                    return (
                        <Fragment>
                            <span className="precioant">${datos.price}</span><span className="preciodesc">${datos.sale_price}</span>
                        </Fragment>
                    );
                }else {
                    return (
                        <span className="preciodesc">${datos.sale_price}</span>
                    );
                }
            }

            return (
                
                <article key={i} className="cuadroProd" data-scarabitem="68337">
                    <div className="cuadroImg">
                        <a href="/producto/67781/+(datos.title)+">
                            <h4><span>{datos.title}</span></h4>
                            <img src={datos.img} />

                            <div className="info"> {precioDescuento()} </div>

                        </a>
                    </div>
                    <div className="cuadroIcons">
                        <div className="lineHeart heartcuadroP">
                            <a href="#modal-wishlist" className="blueHeart2 tooltip tooltip-effect-1" style={{visibility: 'hidden'}}>
                            <span className="tooltip-content">
                                <p>Agregar a lista de deseos</p>
                            </span>
                            </a>
                        </div>
                        <div className="cuadroShare">
                            <div className="shareMe">
                            <ul>
                                <p>Compartir vía:</p>
                                <li><a href="http://www.facebook.com/sharer.php?u=undefined&amp;t=Blusa Con Amarres Philosophy Jr." className="icoFB" target="_blank"></a></li>
                                <li><a href="http://twitter.com/home?status=Leyendo undefined en Sanborns" className="icoTW" target="_blank"></a></li>
                                <li><a href="#compartirEmail" className="icoEM" data-idproducto="68337"></a></li>
                                <li><a href="whatsapp://send?text=undefined" className="icoWS" data-action="share/whatsapp/share"></a></li>
                            </ul>
                            </div>
                        </div>
                    </div>
                </article>
            );
        })


        return (
                <div id="discoveryEm2" className="rowCuadrosProd">
                    {productos}
                </div>
            ); 
            }
        }
                        
export default Productos;