import React from 'react';

//import datosjson from '../jsons/Productos';

//componentes
import Compartir from './Compartir'
import AgregarCarrito from './AgregarCarrito'

//jquery
import $ from 'jquery';

import Slider from 'react-slick';


class SliderView extends React.Component {



    render() {

        const { display, clase, id, productosSlider } = this.props;

        
        // constante que devuelve cada articulo
        //data2
        const productos = productosSlider.map((datos,i)=>{
    
    
            //funcion para poner coma a miles
            const coma =(numero)=>{
                
                numero = numero.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                numero = numero.split('').reverse().join('').replace(/^[\,]/,'');
                //const arr = numero.toString().match(/.{1,4}/g);
                return numero
           
            }
    
            const sinDescuento = ()=>{
                //funcion para saber si un producto tiene descuento
                if(datos.discount > 0){
                    return(
                        <div className="info">
                            <div className="infoDesc"><span className="precioant">$ {coma(datos.price)}</span>       
                                <div className="descText"><span>-{datos.discount}%</span></div>
                            </div><span className="preciodesc">${coma(datos.sale_price)}</span>
                            <div className="infoMensualidades"></div>
                        </div>
                    );
    
                }else{
                    return(
                        <div className="info">
                            <div className="infoDesc"><span className="precioant"></span>       
                            </div><span className="preciodesc">${coma(datos.sale_price)}</span>
                            <div className="infoMensualidades"></div>
                        </div>
                    );
                    
                }
            } 
    
            return(
                    <article key={i} className="productbox slick-slide slick-current slick-active" id="promocion117802"  tabIndex="0" data-slick-index="0" aria-hidden="false">
                        <div className="vistaRapida">
                            <div className="contImgProd"><img src={datos.img} alt="Pantalla Sansui 43&quot; SMX4319USM UHD - Sanborns" title="Pantalla Sansui 43&quot; SMX4319USM UHD - Sanborns"/></div>
                            <a href="/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="linkProducto" tabIndex="0"></a>
                        </div>
                        <div className="carruselContenido">
                            <a href="/producto/117802/pantalla-sansui-43-smx4319usm-uhd/" className="descrip" tabIndex="0">
                                <p>{datos.title}
                                    <br></br>
                                </p>
                            </a>
                            
                            {sinDescuento()}
    
                            <div className="sellIcons">
                                
                                <AgregarCarrito/>

                                <Compartir/>
                                
                            </div>
                        </div>
                    </article>
                    
            );
        });

        var settings = {
            speed: 1500 ,
            slidesToShow: 5,
            slidesToScroll: 5,
            rtl: true,
            infinite: true,
            responsive: [
              {
                breakpoint: 780,
                settings: {
                  slidesToShow: 2,
                  infinite: true,
                  speed: 1500,
                  slidesToScroll: 2
                }
              }
            ]
          };


        return (

            <div className="rowProductos" style={display} id={id}>
                <div className={clase} >
                    <div id="sliderPrincipal">

                        <div className="slick-list draggable">
                            <div className="slick-track" style={{ opacity: '1' }}>

                                <Slider {...settings} >
                                    { productos }

                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SliderView;
