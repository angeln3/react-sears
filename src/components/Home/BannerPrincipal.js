import React from 'react';
//Componentes
import Carrusel from './Carrusel';
import {Link} from 'react-router-dom';

class Footer extends React.Component {
    render() {

        const {productosCaruselP} = this.props;
        
        return (
            <section className="sec__slider">
                <section className="mainSection">
                    <div className="magazineTop">
                        <div id="carruselHomeEmarsys" className="contImgSlider slick-initialized slick-slider">
                            <div className="slick-list draggable">
 
                                <Carrusel productosCaruselP={productosCaruselP}></Carrusel>
                                
                            </div>
                        </div>
                        <div id="carruselHomeEmarsys2" style={{display: 'none'}}></div>
                    </div>
                    <div className="bannersTop">
                        <div className="bannerOne">
                             <Link to="/producto/903195">
                                <img src="https://www.sanborns.com.mx/medios-plazavip/mkt/5dd824c3307c3_ofertasatractivasjpg.jpg"/>
                             </Link>
                        </div>
                        <div className="bannerTwo">
                            <Link to="/producto/158502">    
                                <img src="https://www.sanborns.com.mx/medios-plazavip/mkt/5dc2fc0c65395_libros_sideboxjpg.jpg"/>
                            </Link>
                        </div>
                    </div>
                </section>
            </section>
            
            ); 
            }
        }
                        
export default Footer;
  