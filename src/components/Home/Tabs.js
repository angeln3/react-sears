import React from 'react';
import Fragment from 'render-fragment';

class Tabs extends React.Component {
  render(){  
    return (
      <Fragment>
        <div className="container recomendadosDiscovery"><h2>Te recomendamos</h2></div>
          <section className="categories">
          <div className="categboxes">
              <a href="#andisco" className="boxct active">Todos</a>
              <a href="#andisco" className="boxct">Tecnología y Electrónica </a>
              <a href="#andisco" className="boxct">Entretenimiento</a>
              <a href="#andisco" className="boxct">Videojuegos</a>
              <a href="#andisco" className="boxct">Libros y Revistas</a>
              <a href="#andisco" className="boxct">Perfumes y Maquillaje</a>
              <a href="#andisco" className="boxct">Regalos</a>
              <a href="#andisco" className="boxct">Moda y Belleza</a>
              <a href="#andisco" className="boxct">Juguetes y Dulces</a></div>
            </section>
         </Fragment>
      ); 
    }
  }
  
  export default Tabs;
  