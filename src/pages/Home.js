import React from 'react';
//stilos
//import '../css/styles.css';
//import logo from './logo.svg';
//import '../sass/includes/modulos/header.sass';

 //componentes
//import Footer from './footer/Footer';  
//import Header from './Header/Header';
import Slider2 from '../components/Home/Slider2';
import Tabs from '../components/Home/Tabs';
import BannerPrincipal from '../components/Home/BannerPrincipal';
import SliderSamborns from '../components/Home/SliderSamborns';
import SliderTabs from '../components/Home/SliderTabs';
import ProductosCategorias from '../components/Home/ProductosCategorias';

//Redux
import { connect } from 'react-redux';
//Acciones de Redux
import { 
    showCarrusel,  
    showSlider,
    showSlider2,
    showSliderSamborns,
    showCarruselP,
    showProductosCat
 } from '../actions';

class Home extends React.Component {

    componentWillMount(){
        this.props.showCarrusel();    
        this.props.showSlider();
        this.props.showSlider2();
        this.props.showSliderSamborns();
        this.props.showCarruselP();
        this.props.showProductosCat();
    } 

  render(){  

    const { 
        productosCarrusel,
        productosSlider,
        productosSlider2,
        productosSambrons,
        productosCaruselP,
        productosCat
    
    } = this.props;


    return (
          
        <div className="wrapper">

            <BannerPrincipal productosCaruselP={productosCaruselP}/>

            <SliderTabs productosSlider={productosSlider} productosSlider2={productosSlider2}/>

            <Slider2 clase="contProductoSlider slick-initialized slick-slider" productosCarrusel={productosCarrusel} titulo="Tecnología y Electrónica"/>

            <SliderSamborns productosSambrons={productosSambrons}/> 

            <Tabs/>

            <ProductosCategorias productosCat={productosCat} />

        </div>

    ); 
  }
}
 function mapStateToProps(state){
    return{
        productosCarrusel: state.data.list4,
        productosSlider: state.data.list5,
        productosSlider2: state.data.list6,
        productosSambrons: state.data.list7,
        productosCaruselP: state.data.list8,
        productosCat: state.data.list11
    }
}

export default connect(mapStateToProps, 
    {
      showCarrusel, 
      showSlider,
      showSlider2,
      showSliderSamborns,
      showCarruselP,
      showProductosCat
    })(Home) 