import React from 'react';
//components
import CardTienda from '../components/Tiendas/CardTienda';
import Encabezado from '../components/Tiendas/Encabezado';

//Redux
import { connect } from 'react-redux';
//Acciones de Redux
import { 
    showTiendas
 } from '../actions';

class Tiendas extends React.Component{

    constructor(props){
        super(props);
        this.state ={
            nombreClick: '',
            filtroArreglo : [],
            nombreClickArr: [],
            sinBusqueda: false,
            inputBusqueda: true,
            filtroEstados: [],
            indices: [],
            dataTiendas: []
        }
    }

    componentWillMount(){
        this.props.showTiendas();
    } 

    componentWillReceiveProps(prop){
        this.setState({
            filtroArreglo: prop.dataTiendas
        });
    }



    //filtro por busqueda
    filtroBusqueda = (e) =>{  
        e.preventDefault();

        console.log(e.target.value);

        var qryB = e.target.value;

        // aplicamos el filtro segun lo que ponga en el input
        var fltroSelect =  this.props.dataTiendas.filter(datos =>{
            const regex = new RegExp(qryB, 'gi');
            return (datos.sucursal.match(regex) || datos.tienda.match(regex) ||
                    datos.direccion.match(regex) || datos.horario.match(regex) ||
                    datos.lada.match(regex) || datos.telefono.match(regex) ||
                    datos.estado.match(regex) || datos.cp.match(regex)
                    );
        });

        if (fltroSelect.length < 1){
            this.setState({sinBusqueda: true});
        }else{
            this.setState({sinBusqueda: false});
        }
        //asignamos al filtroArreglo el arreglo que vamos filtrando 
        this.setState({filtroArreglo: fltroSelect});

        //cuando este usando el filtro de Busqueda es true
        this.setState({inputBusqueda: true});
        //limpiamos el filtro de estados
        this.setState({filtroEstados: []});

       

    }

    //filtro agregar varios estados
    filtroXubicacion = (e) =>{

        //nombre del elemento clikeado
        var nombre = e.target.innerHTML;
        //lo asignamos al state para cambiar su estado a active     Object.assign({}, ['a','b','c']
        this.setState({nombreClick: nombre});

        //recorremos los nombres del arreglo para saber si ya ha sido clickeado
        let EdosClikeados = this.state.nombreClickArr;

        //console.log("NOMBRES DEL ARREGLO DE ESTADO",EdosClikeados);

        
        //revisar si esta duplicado y que haga el proceso
        if( EdosClikeados.includes(nombre) == false ){
            //activamos el estado seleccionado
            document.getElementById(nombre).classList.add('active');
            //asigmanos a la lista de arr
            this.state.nombreClickArr.push(nombre);
            // aplicamos el filtro segun el nombre del estado
            var selectEstado =  this.props.dataTiendas.filter(datos => datos.estado == nombre);

            //this.setState({dataTiendas: [...this.state.dataTiendas, Object.assign({}, selectEstado)]});

            console.log('LISTA DE ESTADOS', selectEstado);
            //agregamos a filtroArreglo los filtros que vaya sacando
            selectEstado.map((datos)=>{
                return(
                    this.state.filtroEstados.push(datos)
                );
            });

        }else if(EdosClikeados.includes(nombre) == true ){ 
            this.setState({indices:[]});
            //desactivamos el estado seleccionado
            document.getElementById(nombre).classList.remove('active');
            //encontramos el index del elemento repetido y lo eliminamos
            const index = this.state.nombreClickArr.findIndex(inx => inx === nombre);
            this.state.nombreClickArr.splice(index, 1); 
            //elimanar del arreglo filtroEstados
            var indices = [];
            var indexE = this.state.filtroEstados.map(datos => datos.estado).indexOf(nombre);

            while (indexE != -1) {
                if(indexE !== -1){
                    indices.push(indexE);
                    indexE = this.state.filtroEstados.map(datos => datos.estado).indexOf(nombre, indexE+1);
                    //console.log('INDICES Eliminados',indexE);
                    //this.state.filtroEstados.splice(indexE);
                }
              }
            //console.log('INDICES A ELIMINAR',indices);
            indices.map((datos)=>{
                    delete(this.state.filtroEstados[datos]);
                    //this.state.filtroEstados.splice(datos, 1);
                    //console.log('ELEMNTOS ELIMINADOS', datos);
            });

            
        }
        //cuando ocupamos el filtro por estados el de busqueda es falso
        if(this.state.nombreClickArr.length < 1){
            this.setState({inputBusqueda: true});
        }else{
            this.setState({inputBusqueda: false});
        }
    }

    Sanborns = (e) =>{
        document.getElementById('sanborns').classList.add('active');
        this.setState({inputBusqueda: true});
    }

    

    render(){

        const { 
            dataTiendas
        } = this.props;

        const {
            nombreClick,
            filtroArreglo,
            sinBusqueda,
            inputBusqueda,
            filtroEstados,
            
        } = this.state;

        let arregloConEstadosRepetidos = [];
        //agragamos a la lista de arregloConEstadosRepetidos todos los estados 
        dataTiendas.map((datos)=>{
            arregloConEstadosRepetidos.push(datos.estado)
        });
        //para ponerlos en el filtro de estados sin repetirlos
        let sinEstadosRepetidos = [...new Set(arregloConEstadosRepetidos)].sort();   
        
                
        return(
            <div className="wrapper">

                <Encabezado />
                <section>
                    <div className="container">
                        <div className="contSucursales">
                            <div className="aside">
                                <div className="close-filter">X</div>
                                <form>
                                    <div className="buscaPor">
                                        <h3>Busca por nombre o estado</h3>
                                        <div className="cont-search">
                                            <input type="search" onChange={this.filtroBusqueda}></input>
                                            <button type="button" className="bt__search" onClick={this.Busqueda}></button>
                                        </div>
                                    </div>

                                    <div className="buscaTienda">
                                        <h3>Tienda</h3>

                                        <fieldset id="sanborns" >
                                            <label onClick={this.Sanborns} >sanborns</label>
                                            <input type="checkbox" value="[data-Tienda=sanborns]"  />
                                        </fieldset>

                                        
                                    </div>
                                    <div className="filtroPor">
                                        <h3>Ubicacion</h3> 
                                        
                                            {sinEstadosRepetidos.map((datos)=>{
                                                return (
                                                    
                                                    <fieldset id={datos}>
                                                        <label onClick={this.filtroXubicacion} >{datos}</label>
                                                        <input type="checkbox" value="[data-Tienda=sanborns]"  />
                                                    </fieldset>
                                                );
                                            })}
                                                    
                                            
                                    </div>
                                </form>
                            </div>

                            <div className="contentCards">
                                <span className="active-filter">Filtrar por</span>
                                <div  className="cardAlert" style={{display :  sinBusqueda ? 'block' : 'none' }}>La Busqueda no encontro ningun resultado</div>


                                {inputBusqueda ? filtroArreglo.map((datos)=>{
                                    return(
                                        <CardTienda datos={datos} />
                                    );
                                }): filtroEstados.map((datos)=>{
                                    return(
                                        <CardTienda datos={datos} />
                                    );
                                }) 
                                }

                                
                            </div>
                        </div>
                    </div>
                </section>

            </div>
           
        );
       
    }
}

function mapStateToProps(state){
    return{

        dataTiendas: state.data.datosTiendas
    }
}

export default connect(mapStateToProps, 
    {
      showTiendas
    })(Tiendas) 
