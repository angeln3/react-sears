import React from 'react';
//componentes
import BreadCrumbles from '../components/DetalleProducto/BreadCrumbles'
import MainContainer from '../components/DetalleProducto/MainContainer'
//para las consulta de cadena
import queryString from 'query-string';

//Redux
import { connect } from 'react-redux';

//Acciones de Redux
import {showSliderDP, 
    detalleProducto,
    detalleProductoMigajas, 
    detalleProductoImagenes,
    detalleProductoAttr,
    detalleProductoSliderImg,
    detalleProductoTipo,
    detalleProductoVideos,
    detalleProductoFormasPago,
    detalleProductoColorsANDTallas,
    detalleProductoColor1

    } from '../actions'

class DetalleProducto extends React.Component{

    //resivimos como prop la funcion de los actions
    componentWillMount(){
        //parametros de la url id
        const {id} = this.props.match.params;
        //accion de redux para pasar el id de producto
        this.props.detalleProducto(id);
        this.props.detalleProductoMigajas(id);
        this.props.detalleProductoImagenes(id);
        this.props.detalleProductoAttr(id);
        this.props.detalleProductoSliderImg(id);
        this.props.detalleProductoTipo(id);
        this.props.detalleProductoVideos(id);
        this.props.detalleProductoFormasPago(id);
        this.props.detalleProductoColorsANDTallas(id);
        this.props.detalleProductoColor1(id);

        //parametros del query string
        const query = queryString.parse(this.props.location.search);

        const {skup,skuh} = query;

        //console.log('Esta es el id',id);
        //console.log('Esta es el SKUp',skup,'y skuh',skuh);

        this.props.showSliderDP();

    }

    render(){
        
        //aqui resivimos la info como prop
        const {productosSDP, 
            dataDP, 
            dataDPMig,
            dataDPImg,
            dataDPAttr,
            dataDPSliderImg,
            dataDPTipo,
            dtaDPVid,
            dataDPfpago,
            datosDP_CyT,
            datosDP_colorDef
        } = this.props;

        return(
            <div className="wrapper">
                
                <BreadCrumbles dataDP={dataDP} dataDPMig={dataDPMig} />
                
                <MainContainer 
                productosSDP={productosSDP} 
                dataDP={dataDP} 
                dataDPMig={dataDPMig} 
                dataDPImg={dataDPImg}
                dataDPAttr={dataDPAttr}
                dataDPSliderImg={dataDPSliderImg}
                dataDPTipo={dataDPTipo}
                dtaDPVid={dtaDPVid}
                dataDPfpago={dataDPfpago}
                datosDP_CyT={datosDP_CyT}
                datosDP_colorDef={datosDP_colorDef}
                />
                
            </div>
        );
    }

}

//aqui asignamos el satate de redux a nustro componente
function mapStateToProps(state){
    return {
        productosSDP: state.data.list10,
        dataDP: state.data.datosDP,
        dataDPMig: state.data.datosDP_mig,
        dataDPImg: state.data.datosDP_img, 
        dataDPAttr: state.data.datosDP_attr,
        dataDPSliderImg: state.data.datosDP_slider_img,
        dataDPTipo: state.data.datosDP_tipo,
        dtaDPVid: state.data.datosDP_vid,
        dataDPfpago: state.data.datosDP_imgfpago,
        datosDP_CyT: state.data.datosDP_ColyTall,
        datosDP_colorDef: state.data.datosDP_colorDef
    }
}

//aqui lo conectamos
export default connect(mapStateToProps,
    {showSliderDP, 
    detalleProducto, 
    detalleProductoMigajas,
    detalleProductoImagenes,
    detalleProductoAttr,
    detalleProductoSliderImg,
    detalleProductoTipo,
    detalleProductoVideos,
    detalleProductoFormasPago,
    detalleProductoColorsANDTallas,
    detalleProductoColor1
    })(DetalleProducto);