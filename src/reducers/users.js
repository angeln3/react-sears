//importamos el objeto de Variables de los actions que muestran el contenido
import { VariablesInfo } from '../actions'


const initialState = {
    list: [],
    list2: [],
    list3: [],
    list4: [],
    list5: [],
    list6: [],
    list7: [],
    list8: [],
    list9: [],
    list10: [],
    list11: [],
    list12: [],
    datosDP: [],
    datosDP_mig: [],
    datosDP_img: [],
    datosDP_attr: [],
    datosDP_slider_img: [],
    datosDP_tipo: [],
    datosDP_vid: [],
    datosDP_imgfpago: [],
    datosDP_ColyTall: [],
    datosDP_colorDef: [],
    datosTiendas: []
}

export function showData(state = initialState, action){

    switch(action.type) {

        case VariablesInfo.SHOW_LOGOS:

            return Object.assign({}, state, {list: action.payload});

        case VariablesInfo.SHOW_FOOTER:

            return Object.assign({}, state, {list2: action.payload});
            
        case VariablesInfo.SHOW_FOOTER2:

            return Object.assign({}, state, {list3: action.payload});

        case VariablesInfo.SHOW_CARRUSEL:

            return Object.assign({}, state, {list4: action.payload});

        case VariablesInfo.SHOW_SLIDER:

            return Object.assign({}, state, {list5: action.payload}); 

        case VariablesInfo.SHOW_SLIDER2:

            return Object.assign({}, state, {list6: action.payload});
            
        case VariablesInfo.SHOW_SLIDER_S:

            return Object.assign({}, state, {list7: action.payload});

        case VariablesInfo.SHOW_CARRUSEL_P:

            return Object.assign({}, state, {list8: action.payload});

        case VariablesInfo.SHOW_CATEGORIAS:

            return Object.assign({}, state,{list9: action.payload})

        case VariablesInfo.SHOW_SLIDER_DP:

            return Object.assign({}, state,{list10: action.payload})

        case VariablesInfo.SHOW_PRODUCTOS_CAT:

            return Object.assign({}, state, {list11: action.payload})

        case VariablesInfo.SHOW_EXISTENCIA_TIENDA:

            return Object.assign({}, state, {list12: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO:

            return Object.assign({}, state, {datosDP: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO_MIGAS:

            return Object.assign({}, state, {datosDP_mig: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO_IMG:

            return Object.assign({}, state, {datosDP_img: action.payload})
        
        case VariablesInfo.DETALLE_PRODUCTO_ATTR:

            return Object.assign({}, state, {datosDP_attr: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO_SLIDER_IMG:

            return Object.assign({}, state, {datosDP_slider_img: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO_TIPO:

            return Object.assign({}, state, {datosDP_tipo: action.payload} )

        case VariablesInfo.DETALLE_PRODUCTO_VIDEO:
            
            return Object.assign({}, state, {datosDP_vid: action.payload}) 

        case VariablesInfo.DETALLE_PRODUCTO_FORMAS_PAGO:

            return Object.assign({}, state, {datosDP_imgfpago: action.payload})

        case VariablesInfo.DETALLE_PRODUCTO_COLORS_AND_TALLAS:

            return Object.assign({}, state, {datosDP_ColyTall: action.payload})

        case VariablesInfo.COLOR_DEFAULT:

            return Object.assign({}, state, {datosDP_colorDef: action.payload}) 

        case VariablesInfo.SHOW_TIENDAS:
            
            return Object.assign({}, state, {datosTiendas: action.payload}) 
            
        default:

            return state
    }

}


