import { combineReducers } from 'redux';
import { showData } from './users'
import {nombreEtiqueta} from './stateG'
import {colorSeleccionado} from './stateG'


const rootReducer = combineReducers({
    data: showData,
    nombreETIC : nombreEtiqueta,
    cambioCol : colorSeleccionado
});

export default rootReducer;