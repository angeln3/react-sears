export const nombreEtiqueta = (state = '', action) => {
    switch(action.type){
        case 'CAMBIAR_E':
            return 'Existencia en Tiendas';
        case 'CAMBIAR_E_BTN':
            return '';    
        default:
            return state;
    }
}
const initialState = {
    cambioCOLOR: []
}

export const colorSeleccionado = (state= initialState, action) =>{
    switch(action.type){
        case 'COLOR_SELECCIONADO':
            return Object.assign({}, state, {cambioCOLOR: action.payload});

        default:
                return state;
    }
}